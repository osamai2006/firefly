package com.firefly.android.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.firefly.android.app.R;
import com.firefly.android.app.fragment.MenuDetailsFragment;
import com.firefly.android.app.models.BurgerModel;
import com.firefly.android.app.models.PotatoModel;

import java.util.ArrayList;

/**
 * Created by HP on 5/4/2017.
 */

public class BurgerMeatAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<BurgerModel> arrayList;


    public BurgerMeatAdapter(Context context, ArrayList<BurgerModel> categorieModelArrayList) {
        this.context = context;
        this.arrayList = categorieModelArrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View view = convertView;
        try {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null)
            {
                view = inflater.inflate(R.layout.burger_meat_grid_item_layout, null);

            }

            ImageView potatoradioIMG = (ImageView) view.findViewById(R.id.burgeradioIMG);
            TextView nameTXT= (TextView) view.findViewById(R.id.burgerNameTXT);

            nameTXT.setText(arrayList.get(position).getName());

            if (arrayList.get(position).getChecked().equalsIgnoreCase("true"))
            {
                potatoradioIMG.setBackgroundResource(R.drawable.radio_checked);
            }else
                {
                potatoradioIMG.setBackgroundResource(R.drawable.radio_uncheck);
            }


            potatoradioIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if (arrayList.get(position).getChecked().equalsIgnoreCase("true"))
                    {
                        arrayList.get(position).setChecked("false");

                    }
                    else  if (arrayList.get(position).getChecked().equalsIgnoreCase("false"))
                    {
                        arrayList.get(position).setChecked("true");
                    }

                    MenuDetailsFragment.updateBurgerChecked(position);
                }
            });


            nameTXT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if (arrayList.get(position).getChecked().equalsIgnoreCase("true"))
                    {
                        arrayList.get(position).setChecked("false");

                    }
                    else  if (arrayList.get(position).getChecked().equalsIgnoreCase("false"))
                    {
                            arrayList.get(position).setChecked("true");
                    }

                     MenuDetailsFragment.updateExtraChecked(position);
                }
            });
        }
        catch (Exception xx)
        {}
        return view;
    }

}
