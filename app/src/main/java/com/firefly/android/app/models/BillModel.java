package com.firefly.android.app.models;

import org.json.JSONArray;
import org.json.JSONObject;

public class BillModel {

    private String id;
    private String name;
    private String qty;
    private String price;
    private String notes;
    private JSONArray ingrediantArr;
    private JSONArray extrasArr;
    private JSONObject drinksObj;
    private JSONObject potatoObj;

    public BillModel(String id, String name, String qty, String price, String notes, JSONArray ingrediantArr, JSONArray extrasArr, JSONObject drinksObj, JSONObject potatoObj) {
        this.id = id;
        this.name = name;
        this.qty = qty;
        this.price = price;
        this.notes = notes;
        this.ingrediantArr = ingrediantArr;
        this.extrasArr = extrasArr;
        this.drinksObj = drinksObj;
        this.potatoObj = potatoObj;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public JSONArray getIngrediantArr() {
        return ingrediantArr;
    }

    public void setIngrediantArr(JSONArray ingrediantArr) {
        this.ingrediantArr = ingrediantArr;
    }

    public JSONArray getExtrasArr() {
        return extrasArr;
    }

    public void setExtrasArr(JSONArray extrasArr) {
        this.extrasArr = extrasArr;
    }

    public JSONObject getDrinksObj() {
        return drinksObj;
    }

    public void setDrinksObj(JSONObject drinksObj) {
        this.drinksObj = drinksObj;
    }

    public JSONObject getPotatoObj() {
        return potatoObj;
    }

    public void setPotatoObj(JSONObject potatoObj) {
        this.potatoObj = potatoObj;
    }
}
