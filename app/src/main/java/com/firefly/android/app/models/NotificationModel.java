package com.firefly.android.app.models;

/**
 * Created by HP on 3/25/2018.
 */

public class NotificationModel {

    private String title;
    private String body;
    private String type;
    private String date;

    public NotificationModel(String title, String body, String type, String date) {
        this.title = title;
        this.body = body;
        this.type = type;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
