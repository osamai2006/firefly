package com.firefly.android.app.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.firefly.android.app.R;
import com.firefly.android.app.fragment.SliderFragment;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;

import java.util.Locale;

public class SplashActivity extends AppCompatActivity {
    Animation anim;
    LinearLayout splashLO;
    private static int SPLASH_TIME_OUT = 1500;
    String langFlag;
    SharedPrefsUtils sharedPref;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        anim= AnimationUtils.loadAnimation(this, R.anim.splashanimation);

        splashLO= (LinearLayout) findViewById(R.id.splashLO);
       // splashLO.startAnimation(anim);

        try {
            String loc = "en";
            String currentLoc = sharedPref.getStringPreference(getApplicationContext(), appConstants.locale_KEY);
            if (currentLoc != "") {
                loc = currentLoc;
            }

            if (loc.equalsIgnoreCase("en")) {
                appConstants.current_language = "En";
                Locale locale = new Locale("en");
                Resources res = getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = locale;
                res.updateConfiguration(conf, dm);
                onConfigurationChanged(conf);

            } else {
                appConstants.current_language = "Ar";
                Locale locale = new Locale("ar");
                Resources res = getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = locale;
                res.updateConfiguration(conf, dm);
                onConfigurationChanged(conf);

            }
        }
        catch (Exception xx){}





        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {



                boolean isLogged=sharedPref.getBooleanPreference(SplashActivity.this,appConstants.isLoggedIn,false);

                SliderFragment.editProf=false;
                Intent i = new Intent(SplashActivity.this, CountryActivity.class);
                startActivity(i);
                finish();

//                if(isLogged)
//                {
//                    SliderFragment.editProf=false;
//                    Intent i = new Intent(SplashActivity.this, MainScreenActivity.class);
//                    startActivity(i);
//                    finish();
//                }
//                else
//                {
//                    Intent i = new Intent(SplashActivity.this, loginDetailsActivity.class);
//                    startActivity(i);
//                    finish();
//                }


            }
        }, SPLASH_TIME_OUT);


    }




}
