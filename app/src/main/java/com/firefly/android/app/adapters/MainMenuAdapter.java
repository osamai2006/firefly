package com.firefly.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firefly.android.app.R;
import com.firefly.android.app.fragment.MainMenuFragment;
import com.firefly.android.app.models.MainMenuModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by HP on 8/27/2017.
 */

public class MainMenuAdapter extends RecyclerView.Adapter<MainMenuAdapter.MyViewHolder> {



    Context context;
    String subCatID = "";

    ArrayList<MainMenuModel> arrayList;

    //int pos=0;
    // ProgressDialog progressBarNew;
    public MainMenuAdapter(Context context, ArrayList<MainMenuModel> modelArray) {

        this.context = context;
        this.arrayList = modelArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_menu_grid_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.priceTXT_menu.setText(arrayList.get(position).getPrice()+ " JD");
        holder.mealPriceTXT_menu.setText(arrayList.get(position).getMealPrice()+ " JD");
        holder.nameTXT.setText(arrayList.get(position).getName());
        Picasso.with(context).load(arrayList.get(position).getImage()).into(holder.mealImage_menu);
        //Picasso.with(context).load(arrayList.get(position).getNameImg()).into(holder.nameImg_menu);




        if(arrayList.get(position).getIngrediants().length()>0) {
            JSONArray arr = arrayList.get(position).getIngrediants();
            for (int i = 0; i < arr.length(); i++) {
                try {
                    JSONObject obj = arr.getJSONObject(i);

                    String name = obj.getString("name");

                    if (holder.ingredTXT_menu.getText().toString().equalsIgnoreCase("")) {
                        holder.ingredTXT_menu.setText(name);
                    } else {
                        holder.ingredTXT_menu.setText(holder.ingredTXT_menu.getText().toString() + ", " + name);
                    }


                } catch (Exception xx) {
                }
            }
        }
        holder.mealImage_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainMenuFragment.goToMealDetailsScreen(position);
            }
        });



    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView priceTXT_menu,ingredTXT_menu,mealPriceTXT_menu,nameTXT;
        ImageView mealImage_menu;

        public MyViewHolder(View view) {
            super(view);

            nameTXT = view.findViewById(R.id.nameTXT);
            priceTXT_menu = (TextView) view.findViewById(R.id.priceTXT_menu);
            mealPriceTXT_menu= (TextView) view.findViewById(R.id.mealPriceTXT_menu);
            mealImage_menu = (ImageView) view.findViewById(R.id.mealImage_menu);
            ingredTXT_menu = (TextView) view.findViewById(R.id.ingredTXT_menu);


        }
    }




    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

}