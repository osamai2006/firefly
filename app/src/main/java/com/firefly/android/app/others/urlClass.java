package com.firefly.android.app.others;

/**
 * Created by HP on 5/8/2017.
 */

public class urlClass
{

    public static String  baseURL="http://api.burgerfirefly.com/v2/";
    //public static String  baseURL="http://192.168.8.105/hrAPI/Service1.svc/";

    public static String registerURL=baseURL+"register";
    public static String loginURL=baseURL+"login";
    public static String verfiyOtpURL=baseURL+"otp/verify";
    public static String resendOtpURL=baseURL+"otp/resend";
    public static String getProfileURL=baseURL+"user/";
    public static String updateProfileURL=baseURL+"user/";
    public static String getMainMenuURL=baseURL+"menu/items";
    public static String getenuDetailsURL=baseURL+"item";
    public static String addToCartURL=baseURL+"add-to-cart";
    public static String updateCartURL=baseURL+"cart/update";
    public static String getCartURL=baseURL+"my-cart";
    public static String getFaqURL=baseURL+"faq";
    public static String contactUsURL=baseURL+"contact-us";
    public static String getAboutUsURL=baseURL+"about-us";
    public static String getCountryURL=baseURL+"countries";
    public static String getCityURL=baseURL+"cities";
    public static String getBranchsURL=baseURL+"branches";
    public static String checkOutDininPickupURL=baseURL+"cart/checkout";
    public static String deleverycheckOutURL=baseURL+"cart/checkout";
    public static String placeOrderURL=baseURL+"cart/place-order";
    public static String getBillURL=baseURL+"order/";
    public static String reOrderURL=baseURL+"order/reorder";
    public static String getPointsURL=baseURL+"user/points";
    public static String getMyOrderURL=baseURL+"my-orders";
    public static String getDeleteFromCartURL=baseURL+"cart/item-delete";
    public static String getUserAddressURL=baseURL+"user/address";
    public static String getNotificationURL=baseURL+"notifications";
    public static String addUserAddressURL=baseURL+"user/delivery-address";
    public static String deleteAddressURL=baseURL+"user/";
    public static String uploadPhotoURL=baseURL+"user/photo";
    public static String logoutURL=baseURL+"logout";
    public static String getSliderURL=baseURL+"slider";
    public static String loginSocialMediaURL=baseURL+"loginSocialMediaURL";
    public static String worldMapURL=baseURL+"world-map";






}