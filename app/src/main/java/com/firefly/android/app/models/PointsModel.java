package com.firefly.android.app.models;

/**
 * Created by HP on 3/25/2018.
 */

public class PointsModel {

    private String points;
    private String balance;
    private String date;

    public PointsModel(String points, String balance, String date) {
        this.setPoints(points);
        this.setBalance(balance);
        this.setDate(date);
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
