package com.firefly.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firefly.android.app.R;
import com.firefly.android.app.fragment.MyCartFragment;
import com.firefly.android.app.models.MyCartModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by HP on 8/27/2017.
 */

public class MyCartRecycleAdapter_new extends RecyclerView.Adapter<MyCartRecycleAdapter_new.MyViewHolder> {


    Context context;
    ArrayList<MyCartModel> arrayList;


    public MyCartRecycleAdapter_new(Context context, ArrayList<MyCartModel> modelArray) {

        this.context = context;
        this.arrayList = modelArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            View itemView = LayoutInflater.from(context).inflate(R.layout.cart_grid_item_layout_new, parent, false);

            return new MyViewHolder(itemView);
        }
        catch (Exception xx)
        {
            xx.getMessage();
            return null;
        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.nameTXT.setText(arrayList.get(holder.getAdapterPosition()).getName());
        holder.mealPriceTXT_menu.setText(arrayList.get(holder.getAdapterPosition()).getPrice());
        //holder.qtyCartTXT.setText(arrayList.get(holder.getAdapterPosition()).getQuentity());
        Picasso.with(context).load(arrayList.get(holder.getAdapterPosition()).getImage()).into(holder.mealImage_menu);



        if(arrayList.get(position).getIngrediantArr().length()>0) {
            JSONArray arr = arrayList.get(position).getIngrediantArr();
            for (int i = 0; i < arr.length(); i++) {
                try {
                    JSONObject obj = arr.getJSONObject(i);

                    String name = obj.getString("name");

                    if (holder.ingredTXT_menu.getText().toString().equalsIgnoreCase("")) {
                        holder.ingredTXT_menu.setText(name);
                    } else {
                        holder.ingredTXT_menu.setText(holder.ingredTXT_menu.getText().toString() + ", " + name);
                    }


                } catch (Exception xx) {
                }
            }
        }

//        if(arrayList.get(holder.getAdapterPosition()).getExtrasArr().length()>0)
//        {
//            JSONArray arr=arrayList.get(holder.getAdapterPosition()).getExtrasArr();
//            for(int i=0;i<arr.length();i++)
//            {
//                try {
//                    JSONObject obj = arr.getJSONObject(i);
//
//                    String name = obj.getString("name");
//                    holder.extrasTXT.setText(name+",");
//
//                }
//                catch (Exception xx){}
//            }
//            if( holder.extrasTXT.getText().toString().endsWith(","))
//            {
//                holder.extrasTXT.setText(holder.extrasTXT.getText().toString().substring(0, holder.extrasTXT.getText().toString().length() - 1));
//            }
//        }

//        if(arrayList.get(position).getIngrediantArr().length()>0) {
//            JSONArray arr = arrayList.get(position).getExtrasArr();
//            for (int i = 0; i < arr.length(); i++) {
//                try {
//                    JSONObject obj = arr.getJSONObject(i);
//
//                    String name = obj.getString("name");
//
//                    if (holder.extrasTXT.getText().toString().equalsIgnoreCase("")) {
//                        holder.extrasTXT.setText(name);
//                    } else {
//                        holder.extrasTXT.setText(holder.extrasTXT.getText().toString() + ", " + name);
//                    }
//
//
//                } catch (Exception xx) {
//                }
//            }
//        }





    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTXT,mealPriceTXT_menu,ingredTXT_menu;
        ImageView mealImage_menu;

        public MyViewHolder(View view) {
            super(view);

            nameTXT =  view.findViewById(R.id.nameTXT);
            mealPriceTXT_menu =  view.findViewById(R.id.mealPriceTXT_menu);
            ingredTXT_menu=  view.findViewById(R.id.ingredTXT_menu);
            //qtyCartTXT=  view.findViewById(R.id.qtyCartTXT);
            mealImage_menu =  view.findViewById(R.id.mealImage_menu);




        }
    }




    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

}