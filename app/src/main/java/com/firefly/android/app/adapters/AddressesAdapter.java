package com.firefly.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.firefly.android.app.R;
import com.firefly.android.app.fragment.AddressesFragmnet;
import com.firefly.android.app.fragment.MyCartFragment;
import com.firefly.android.app.models.AddressesModel;
import com.firefly.android.app.models.MyCartModel;
import com.firefly.android.app.others.appConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by HP on 8/27/2017.
 */

public class AddressesAdapter extends RecyclerView.Adapter<AddressesAdapter.MyViewHolder> {



    Context context;
    ArrayList<AddressesModel> arrayList;


    public AddressesAdapter(Context context, ArrayList<AddressesModel> modelArray) {

        this.context = context;
        this.arrayList = modelArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.addresses_grid_item_layout, parent, false);

            return new MyViewHolder(itemView);
        }
        catch (Exception xx)
        {
            xx.getMessage();
            return null;
        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.addressNameTXT.setText(arrayList.get(position).getName());


        holder.removeBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              AddressesFragmnet.removeAddressReq(arrayList.get(position).getId(),position);

            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(AddressesFragmnet.comeFromCart.equalsIgnoreCase("1"))
                {
                    appConstants.addressID=arrayList.get(position).getId();
                    AddressesFragmnet.goToConfirmScreen();
                }
                else
                {
                    appConstants.addressID="";
                }
            }
        });




    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView addressNameTXT;
        Button removeBTN;


        public MyViewHolder(View view) {
            super(view);

            addressNameTXT = (TextView) view.findViewById(R.id.addressNameTXT);
            removeBTN = (Button) view.findViewById(R.id.removeBTN);


        }
    }




    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

}