package com.firefly.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firefly.android.app.R;
import com.firefly.android.app.fragment.MyCartFragment;
import com.firefly.android.app.models.MyCartModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by HP on 8/27/2017.
 */

public class MyCartRecycleAdapter extends RecyclerView.Adapter<MyCartRecycleAdapter.MyViewHolder> {


    Context context;
    ArrayList<MyCartModel> arrayList;


    public MyCartRecycleAdapter(Context context, ArrayList<MyCartModel> modelArray) {

        this.context = context;
        this.arrayList = modelArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            View itemView = LayoutInflater.from(context).inflate(R.layout.my_cart_grid_item_layout, parent, false);

            return new MyViewHolder(itemView);
        }
        catch (Exception xx)
        {
            xx.getMessage();
            return null;
        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.cartMealNameTXT.setText(arrayList.get(holder.getAdapterPosition()).getName());
        holder.cartPriceTXT.setText(arrayList.get(holder.getAdapterPosition()).getPrice()+" JD");
        holder.qtyCartTXT.setText(arrayList.get(holder.getAdapterPosition()).getQuentity());
        Picasso.with(context).load(arrayList.get(holder.getAdapterPosition()).getImage()).into(holder.cartIMG);



        if(arrayList.get(position).getIngrediantArr().length()>0) {
            JSONArray arr = arrayList.get(position).getIngrediantArr();
            for (int i = 0; i < arr.length(); i++) {
                try {
                    JSONObject obj = arr.getJSONObject(i);

                    String name = obj.getString("name");

                    if (holder.ingredaitsTXT.getText().toString().equalsIgnoreCase("")) {
                        holder.ingredaitsTXT.setText(name);
                    } else {
                        holder.ingredaitsTXT.setText(holder.ingredaitsTXT.getText().toString() + ", " + name);
                    }


                } catch (Exception xx) {
                }
            }
        }

//        if(arrayList.get(holder.getAdapterPosition()).getExtrasArr().length()>0)
//        {
//            JSONArray arr=arrayList.get(holder.getAdapterPosition()).getExtrasArr();
//            for(int i=0;i<arr.length();i++)
//            {
//                try {
//                    JSONObject obj = arr.getJSONObject(i);
//
//                    String name = obj.getString("name");
//                    holder.extrasTXT.setText(name+",");
//
//                }
//                catch (Exception xx){}
//            }
//            if( holder.extrasTXT.getText().toString().endsWith(","))
//            {
//                holder.extrasTXT.setText(holder.extrasTXT.getText().toString().substring(0, holder.extrasTXT.getText().toString().length() - 1));
//            }
//        }

        if(arrayList.get(position).getIngrediantArr().length()>0) {
            JSONArray arr = arrayList.get(position).getExtrasArr();
            for (int i = 0; i < arr.length(); i++) {
                try {
                    JSONObject obj = arr.getJSONObject(i);

                    String name = obj.getString("name");

                    if (holder.extrasTXT.getText().toString().equalsIgnoreCase("")) {
                        holder.extrasTXT.setText(name);
                    } else {
                        holder.extrasTXT.setText(holder.extrasTXT.getText().toString() + ", " + name);
                    }


                } catch (Exception xx) {
                }
            }
        }



        holder.removeCartBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyCartFragment.removeCartReq(arrayList.get(holder.getAdapterPosition()).getCartItemId(),position);

            }
        });

        holder.updateCartBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyCartFragment.goToEditCartScreen(position);

            }
        });



    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView cartMealNameTXT,cartPriceTXT,ingredaitsTXT,extrasTXT,qtyCartTXT;
        ImageView cartIMG,removeCartBTN,updateCartBTN;

        public MyViewHolder(View view) {
            super(view);

            cartMealNameTXT =  view.findViewById(R.id.cartMealNameTXT);
            cartPriceTXT =  view.findViewById(R.id.cartPriceTXT);
            ingredaitsTXT=  view.findViewById(R.id.ingredaitsTXT);
            extrasTXT=  view.findViewById(R.id.extrasTXT);
            qtyCartTXT=  view.findViewById(R.id.qtyCartTXT);
            cartIMG =  view.findViewById(R.id.cartIMG);
            updateCartBTN=  view.findViewById(R.id.updateCartBTN);
            removeCartBTN= view.findViewById(R.id.removeCartBTN);



        }
    }




    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

}