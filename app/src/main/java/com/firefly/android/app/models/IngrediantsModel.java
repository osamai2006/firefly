package com.firefly.android.app.models;

public class IngrediantsModel {

    private String id;
    private String image;
    private String name;
    private String checked;

    public IngrediantsModel(String id, String image, String name, String checked) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.checked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
}
