package com.firefly.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firefly.android.app.R;
import com.firefly.android.app.fragment.MyCartFragment;
import com.firefly.android.app.models.BillModel;
import com.firefly.android.app.models.ExtrasModel;
import com.firefly.android.app.models.MyCartModel;
import com.firefly.android.app.others.appConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by HP on 8/27/2017.
 */

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder> {



    Context context;
    ArrayList<BillModel> arrayList;

    static InvoiceExtrasAdapter extraAdapter;
    static ArrayList<ExtrasModel> extraArrayList;
    ExtrasModel extrasModel;

    //int pos=0;
    // ProgressDialog progressBarNew;
    public InvoiceAdapter(Context context, ArrayList<BillModel> modelArray) {

        this.context = context;
        this.arrayList = modelArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.invoice_grid_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.invMealNameTXT.setText(arrayList.get(position).getName());
        holder.invQtyTXT.setText(arrayList.get(position).getQty());
        holder.invSubPriceTXT.setText(arrayList.get(position).getPrice()+ " JD");


//        if(arrayList.get(position).getIngrediantArr().length()>0)
//        {
//            JSONArray arr=arrayList.get(position).getIngrediantArr();
//            for(int i=0;i<arr.length();i++)
//            {
//                try {
//                    JSONObject obj = arr.getJSONObject(i);
//
//                    String name = obj.getString("name");
//                    holder.invIngredaitsTXT.setText(name+",");
//
//                }
//                catch (Exception xx){}
//            }
//            if( holder.invIngredaitsTXT.getText().toString().endsWith(","))
//            {
//                holder.invIngredaitsTXT.setText(holder.invIngredaitsTXT.getText().toString().substring(0, holder.invIngredaitsTXT.getText().toString().length() - 1));
//            }
//        }

        if(arrayList.get(position).getIngrediantArr().length()>0) {
            JSONArray arr = arrayList.get(position).getIngrediantArr();
            for (int i = 0; i < arr.length(); i++) {
                try {
                    JSONObject obj = arr.getJSONObject(i);

                    String name = obj.getString("name");

                    if (holder.invIngredaitsTXT.getText().toString().equalsIgnoreCase("")) {
                        holder.invIngredaitsTXT.setText(name);
                    } else {
                        holder.invIngredaitsTXT.setText(holder.invIngredaitsTXT.getText().toString() + ", " + name);
                    }


                } catch (Exception xx) {
                }
            }
        }


        try
        {
            if(arrayList.get(position).getExtrasArr().length()>0)
            {
                extraArrayList=new ArrayList<>();
                JSONArray extrasArr=arrayList.get(position).getExtrasArr();

                for (int i = 0; i < extrasArr.length(); i++) {
                    JSONObject obj = extrasArr.getJSONObject(i);
                    String id = obj.getString("id");
                    String name = obj.getString("name");
                    String price = obj.getString("price");



                    extrasModel = new ExtrasModel(id, "", name, price, "");
                    extraArrayList.add(extrasModel);


                }

                extraAdapter = new InvoiceExtrasAdapter(context,extraArrayList);
                holder.invExtrasLV.setAdapter(extraAdapter);
                LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                holder.invExtrasLV.setLayoutManager(layoutManager);
            }

        }
        catch(Exception xx){}

    }






    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView invMealNameTXT,invQtyTXT,invSubPriceTXT,invIngredaitsTXT;
        RecyclerView invExtrasLV;

        public MyViewHolder(View view) {
            super(view);

            invMealNameTXT = (TextView) view.findViewById(R.id.invMealNameTXT);
            invQtyTXT = (TextView) view.findViewById(R.id.invQtyTXT);
            invSubPriceTXT= (TextView) view.findViewById(R.id.invSubPriceTXT);
            invIngredaitsTXT= (TextView) view.findViewById(R.id.invIngredaitsTXT);
            invExtrasLV= (RecyclerView) view.findViewById(R.id.invExtrasLV);


        }
    }




    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

}