package com.firefly.android.app.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;

import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.adapters.MyCartRecycleAdapter;
import com.firefly.android.app.adapters.MyCartRecycleAdapter_new;
import com.firefly.android.app.models.MyCartModel;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 5/3/2018.
 */

public class MyCartFragment extends Fragment  {

    RecyclerView my_cartLV;
    Dialog deleviryDialog;

    View view;


    static ArrayList<MyCartModel> arrayList;
    MyCartModel model;
    static MyCartRecycleAdapter_new adapter;
    static Button checkOutBTN;
    public static TextView totalTXT;
    //public static LinearLayout totalLO;

    static FragmentTransaction tx;
    private static Fragment fragment;


    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.my_cart_fragment_layout, container, false);
        my_cartLV= view.findViewById(R.id.my_cartLV);
        totalTXT= view.findViewById(R.id.totalTXT);
//        totalLO= view.findViewById(R.id.totalLO);
//        totalLO.setVisibility(View.GONE);

        checkOutBTN=view.findViewById(R.id.checkoutBTN);
        checkOutBTN.setVisibility(View.GONE);


        MainScreenActivity.toolbar_title.setText(R.string.my_cart);


        checkOutBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConfirmationFragmentNew.price=totalTXT.getText().toString().trim();

                showDeleveryMethodPop();


            }
        });

        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getCartURL;
                    getCartReq(url, null);

                } catch (Exception xx)
                {

                }
            }
        }).start();




        return view;
    }

    private void getCartReq(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        arrayList=new ArrayList<>();


                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONObject mainObject=jsonObjResp.getJSONObject("data");
                            JSONArray itemsArr=mainObject.getJSONArray("items");

                            String cartIDd=mainObject.getString("cartId");

                            String total=mainObject.getString("total");
                            checkOutBTN.setVisibility(View.VISIBLE);
                            //totalLO.setVisibility(View.VISIBLE);

                            for (int i=0;i<itemsArr.length();i++)
                            {
                                JSONObject obj=itemsArr.getJSONObject(i);

                                String itemID=obj.getString("itemId");
                                String cartItemId=obj.getString("cartItemId");
                                String name=obj.getString("name");
                                String img=obj.getString("itemImage");
                                String price=obj.getString("price");
                                String qty=obj.getString("quantity");
                                String itemType=obj.getString("itemType");
                                JSONArray ingrediant=obj.getJSONArray("ingredients");
                                JSONArray extra=new JSONArray();
                                try
                                {
                                     extra=obj.getJSONArray("addons");
                                }
                                catch (Exception xx)
                                {

                                }

                                model=new MyCartModel(itemID,cartItemId,img,name,price,qty,ingrediant,extra,itemType);
                                arrayList.add(model);

                            }

                            adapter = new MyCartRecycleAdapter_new(getActivity(),arrayList);
                            my_cartLV.setAdapter(adapter);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            my_cartLV.setLayoutManager(layoutManager);

                            calculateCart();
                            if(arrayList.size()<=0)
                            {
                                checkOutBTN.setVisibility(View.GONE);
                                //totalLO.setVisibility(View.GONE);
                            }

                        }


                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        checkOutBTN.setVisibility(View.GONE);
                        //totalLO.setVisibility(View.GONE);
                        //Toast.makeText(getActivity(), "server error", Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    checkOutBTN.setVisibility(View.GONE);
                   // totalLO.setVisibility(View.GONE);
                    //Toast.makeText(getActivity(),"server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                checkOutBTN.setVisibility(View.GONE);
                //totalLO.setVisibility(View.GONE);
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }


    public static void removeCartReq(final String itemID, final int pos) {

        appConstants.startSpinwheel(myContext, false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonObjectRequest jsonObjReq;
                JSONObject obj=new JSONObject();
                try
                {
                    obj.putOpt("cartItemId",itemID);
                }
                catch (Exception xx){}

                jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlClass.getDeleteFromCartURL, obj, new Response.Listener<JSONObject>()
                {

                    @Override
                    public void onResponse(JSONObject response) {

                        try
                        {
                            if(response !=null)
                            {
                                appConstants.stopSpinWheel();

                                JSONObject jsonObjResp = new JSONObject(response.toString());
                                final String status=jsonObjResp.getString("success");

                                if(status.equalsIgnoreCase("true"))
                                {
                                    arrayList.remove(pos);
                                    adapter.notifyDataSetChanged();

                                }
                                calculateCart();
                                MainScreenActivity.getCartCountReq();

                            }
                            else
                            {
                                appConstants.stopSpinWheel();
                                Toast.makeText(myContext, myContext.getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            }


                        }
                        catch (Exception e)
                        {
                            Toast.makeText(myContext,myContext.getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            appConstants.stopSpinWheel();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Toast.makeText(myContext,myContext.getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                        appConstants.stopSpinWheel();

                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        String bearer = "Bearer ".concat(appConstants.userToken);
                        Map<String, String> headersSys = super.getHeaders();
                        Map<String, String> headers = new HashMap<String, String>();
                        headersSys.remove("Authorization");
                        headers.put("Authorization", bearer);
                        headers.put("language", appConstants.current_language);
                        headers.putAll(headersSys);
                        return headers;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(myContext);
                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                        appConstants.appTimeOut,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(jsonObjReq);
            }
        }).start();

    }


    public static void calculateCart()
    {
        double total=0;
        if(arrayList.size()<=0)
        {
            checkOutBTN.setVisibility(View.GONE);
            //totalLO.setVisibility(View.GONE);
            totalTXT.setText(String.valueOf(total)+ " JD");

        }
        else
            {
            for (int i = 0; i < arrayList.size(); i++) {
                try {
                    double price = Double.parseDouble(arrayList.get(i).getPrice());
                    total += price;
                } catch (Exception xx) {
                }
            }
                totalTXT.setText(String.valueOf(total)+ " JD");
        }


    }

    private void showDeleveryMethodPop() {

        deleviryDialog = new Dialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
        deleviryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        deleviryDialog.setContentView(R.layout.checkout_pop_dialog);
        deleviryDialog.setCanceledOnTouchOutside(false);
        deleviryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        RadioGroup deleveryRG= deleviryDialog.findViewById(R.id.deleviryRG);

        deleveryRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId==R.id.dineInRB)
                {
                    DinInPickupFragment.checkoutMethod="2";

                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new DinInPickupFragment());
                    tx.addToBackStack(null);
                    tx.commit();

                    deleviryDialog.dismiss();

                }

                if(checkedId==R.id.takeAwayRB)
                {
                    DinInPickupFragment.checkoutMethod="3";

                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new DinInPickupFragment());
                    tx.addToBackStack(null);
                    tx.commit();

                    deleviryDialog.dismiss();
                }

                if(checkedId==R.id.delevryRB)
                {
                    //DeliveryFragmentNew.checkoutMethod="1";

                    AddressesFragmnet.comeFromCart="1";

                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new AddressesFragmnet());
                    tx.addToBackStack(null);
                    tx.commit();

//                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
//                    tx.replace(R.id.main_container, new DeliveryFragmentNew());
//                    tx.addToBackStack(null);
//                    tx.commit();

                    deleviryDialog.dismiss();

                }


            }
        });


        deleviryDialog.show();

    }

    public static void goToEditCartScreen(int position)
    {

        EditCartFragmentNew.name=arrayList.get(position).getName();
        EditCartFragmentNew.totalPrice=arrayList.get(position).getPrice();
        EditCartFragmentNew.image=arrayList.get(position).getImage();
        EditCartFragmentNew.mealID=arrayList.get(position).getId();
        EditCartFragmentNew.qty=Integer.parseInt(arrayList.get(position).getQuentity());
        EditCartFragmentNew.mealSandwichFlag=arrayList.get(position).getItemType();



        tx = myContext.getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main_container, new EditCartFragmentNew());
        tx.addToBackStack(null);
        tx.commit();
    }


}
