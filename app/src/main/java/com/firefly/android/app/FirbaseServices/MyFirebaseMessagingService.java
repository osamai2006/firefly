package com.firefly.android.app.FirbaseServices;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.firefly.android.app.fragment.SliderFragment;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;

import org.json.JSONObject;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    SharedPrefsUtils sharedPref;
    String data, body, title;
    PendingIntent pendingIntent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        try {
            //Map<String, String> dataDic = remoteMessage.getData();
            JSONObject obj=new JSONObject(remoteMessage.getData());

            body = obj.getString("body");
            title = obj.getString("title");
            data = obj.getString("type");

        }
        catch (Exception xx)
        {
        }


        showNotification(body,data);
    }

    private void showNotification(String messageBody,String messageData) {


        try {


            if(data!=null)
            {
                if (!data.equalsIgnoreCase("")) {

                    SliderFragment.editProf=true;
                    Intent intent = new Intent(this, MainScreenActivity.class);
                    intent.putExtra("type", messageData);
                    pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

                } else {
                    SliderFragment.editProf=true;
                    Intent intent = new Intent(this, MainScreenActivity.class);
                    intent.putExtra("offerId", "");
                    pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                }
            }

            Bitmap notifyImage = BitmapFactory.decodeResource(getResources(), R.mipmap.icon);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.icon)
                    .setLargeIcon(notifyImage)
                    .setColor(Color.parseColor("#FFE74C3C"))
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

                NotificationChannel channel = new NotificationChannel("default",getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
                channel.setDescription(messageBody);
                notificationManager.createNotificationChannel(channel);
                notificationBuilder.setChannelId("default");

            }

            notificationManager.notify(0, notificationBuilder.build());
        }
        catch (Exception xx){}
    }
}