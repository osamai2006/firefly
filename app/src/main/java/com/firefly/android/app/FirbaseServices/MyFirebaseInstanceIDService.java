package com.firefly.android.app.FirbaseServices;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONObject;


/**
 * Created by Ayadi on 3/20/2017.
 */



public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();
    SharedPrefsUtils sharedPref;

    @Override
    public void onTokenRefresh()
    {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sharedPref.setStringPreference(getApplicationContext(), appConstants.mobileToken_KEY,refreshedToken);

        try {
            String userID = sharedPref.getStringPreference(getApplicationContext(), appConstants.userID_KEY);
            JSONObject Objct = new JSONObject();
            Objct.putOpt("id", userID);
            Objct.putOpt("mobile_token", refreshedToken);
            Objct.putOpt("mobile_type", "1");

            postTokenRequest(getApplicationContext(),"", Objct);
        }
        catch (Exception xx){}


        Log.d("tokenDevice", refreshedToken);
        // Saving reg id to shared preferences
        //storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        //sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(ConfigFCM.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    public static void postTokenRequest(Context contxt, String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("status");

                        if(status.equalsIgnoreCase("true"))
                        {


                        }

                    }



                }
                catch (Exception e)
                {
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                appConstants.stopSpinWheel();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(contxt);
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

//    private void sendRegistrationToServer(final String token) {
//        // sending gcm token to server
//        Log.e(TAG, "sendRegistrationToServer: " + token);
//        int login_id = mypref.getIntegerPreference(this, sessionClass.Login_Id, 0);
//        Log.d("login_id", "" + login_id);
//        if (login_id == 0) {
//
//        } else {
//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.putOpt("Device_IMEI", Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID));
//                jsonObject.putOpt("DeviceToken", token);
//                jsonObject.putOpt("Platform", 0);
//                jsonObject.putOpt("Login_I", login_id);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            String url = "http://modarescom.com/api/Users/AddDeviceToken";
//            RequestQueue queue = Volley.newRequestQueue(this);
//            JsonObjectRequest jsObjRequest = new JsonObjectRequest(
//                    Request.Method.POST, url, jsonObject,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            Log.e("doneToken", "Response : Error Device Registration" + response.toString());
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Log.e("errorToken", "Response : Error Device Registration" + error.getMessage());
//                }
//            });
//            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
//                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, 2, 2));
//            queue.add(jsObjRequest);
//}
//    }
//    private void storeRegIdInPref(String token) {
//        SharedPreferences pref = getApplicationContext().getSharedPreferences(ConfigFCM.SHARED_PREF, 0);
//        SharedPreferences.Editor editor = pref.edit();
//        editor.putString("regId", token);
//        editor.commit();
//        mypref.setStringPreference(this, sessionClass.token, token);
//    }
}