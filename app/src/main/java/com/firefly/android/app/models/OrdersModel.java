package com.firefly.android.app.models;

import org.json.JSONArray;

/**
 * Created by HP on 3/25/2018.
 */

public class OrdersModel {

    private String id;
    private String number;
    private String date;
    private String time;
    private String type;
    private String price;
    private String tax;
    private String total;
    private String qty;
    private JSONArray  itemsArr;

    public OrdersModel(String id, String number, String date, String time, String type, String price, String tax, String total, String qty, JSONArray itemsArr) {
        this.setId(id);
        this.setNumber(number);
        this.setDate(date);
        this.setTime(time);
        this.setType(type);
        this.setPrice(price);
        this.setTax(tax);
        this.setTotal(total);
        this.setQty(qty);
        this.setItemsArr(itemsArr);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public JSONArray getItemsArr() {
        return itemsArr;
    }

    public void setItemsArr(JSONArray itemsArr) {
        this.itemsArr = itemsArr;
    }
}
