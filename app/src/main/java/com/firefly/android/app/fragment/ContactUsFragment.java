package com.firefly.android.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 5/9/2017.
 */

public class ContactUsFragment extends Fragment  {

    View view;
    EditText emailTXT,subject_contact_txt,mesgTXT;
    Button submitBTN;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact_us_layout, container, false);

        subject_contact_txt=view.findViewById(R.id.subject_contact_txt);
        emailTXT=view.findViewById(R.id.email_contact_txt);
        mesgTXT=view.findViewById(R.id.mesg_contacttxt);

        MainScreenActivity.toolbar_title.setText(R.string.contact_dr);

        submitBTN =view.findViewById(R.id.submit_contactBTN);
        submitBTN.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(checkRule()) {
                    appConstants.startSpinwheel(getActivity(), false, true);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                JSONObject objct = new JSONObject();
                                objct.putOpt("subject", subject_contact_txt.getText().toString().trim());
                                objct.putOpt("email", emailTXT.getText().toString().trim());
                                objct.putOpt("body", mesgTXT.getText().toString().trim());

                                postContactusRequest(urlClass.contactUsURL, objct);

                            } catch (Exception xx) {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }
                        }
                    }).start();
                }
            }
        });



        return view;
    }
    private  boolean checkRule()
    {
        if (subject_contact_txt.getText().toString().trim().equals(""))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (emailTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!emailTXT.getText().toString().trim().matches(appConstants.email_pattern)) {
            Toast.makeText(getActivity(), "Wrong email", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (mesgTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;


    }

    private void postContactusRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            Toast.makeText(getActivity(), "Thank You", Toast.LENGTH_LONG).show();
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }


}
