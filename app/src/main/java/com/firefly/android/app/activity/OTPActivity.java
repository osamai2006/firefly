package com.firefly.android.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firefly.android.app.R;
import com.firefly.android.app.fragment.SliderFragment;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.VolleyRequestClass;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;
import com.mukesh.countrypicker.fragments.CountryPicker;
import com.mukesh.countrypicker.interfaces.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import static com.firefly.android.app.others.appConstants.appTimeOut;
import static com.firefly.android.app.others.appConstants.otp;
import static com.firefly.android.app.others.appConstants.stopSpinWheel;

/**
 * Created by HP on 5/3/2017.
 */

public class OTPActivity extends Activity implements VolleyRequestClass.ResponseHandler{
    EditText otpTXT;
    TextView mobNumTXT;
    Button loginBTN,resenBTN,editMobileBTN;
    SharedPrefsUtils sharedPref;
   public static String phone="";


    VolleyRequestClass requestClass=new VolleyRequestClass();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_layout);

        requestClass.responseHandler=this;




        otpTXT= (EditText)findViewById(R.id.otpTXT);
        loginBTN= (Button) findViewById(R.id.lognBTN);
        resenBTN= (Button) findViewById(R.id.resenBTN);
        editMobileBTN= (Button) findViewById(R.id.editMobileBTN);
        mobNumTXT=  findViewById(R.id.mobNumTXT);
        mobNumTXT.setText(getString(R.string.your_mobile)+"\n"+phone);

        loginBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                try
                {

                    if(checkRule())
                    {
                        appConstants.startSpinwheel(OTPActivity.this, false, true);

                        new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try
                                            {
                                            String token=sharedPref.getStringPreference(OTPActivity.this,appConstants.mobileToken_KEY);
                                            String url= urlClass.verfiyOtpURL;

                                            JSONObject Objct = new JSONObject();
                                            Objct.putOpt("phone", phone);
                                            Objct.putOpt("otp", otpTXT.getText().toString().trim());
                                            Objct.putOpt("deviceToken",token);

                                            //loginRequestOld(url,Objct);
                                            requestClass.objectRequest(Objct,url,"otp",getApplicationContext());
                                        }
                                        catch (Exception xx)
                                        {
                                            xx.toString();
                                            stopSpinWheel();
                                        }

                                        }
                                    }).start();






                    }
                }
                catch (Exception xx)
                {
                    xx.toString();
                }
            }
        });



        editMobileBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
               finish();
            }
        });


        resenBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try
                {
                    String url= urlClass.resendOtpURL;

                    JSONObject Objct = new JSONObject();
                    Objct.putOpt("phone", phone);

                    requestClass.objectRequest(Objct,url,"resend",getApplicationContext());

                }
                catch (Exception xx)
                {
                    xx.toString();
                    stopSpinWheel();
                }
            }
        });

    }

    private  boolean checkRule() {

        if (otpTXT.getText().toString().trim().equals("")) {
            Toast.makeText(OTPActivity.this, getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!otpTXT.getText().toString().trim().equals(otp)) {
            Toast.makeText(OTPActivity.this, getResources().getString(R.string.wrong_otp), Toast.LENGTH_SHORT).show();
            return false;
        }

        return  true;
    }




    @Override
    public void successRequest(JSONObject response, String apiName) {

        if(apiName.equalsIgnoreCase("otp"))
        {
            try
            {
                appConstants.stopSpinWheel();

                JSONObject jsonObjResp = new JSONObject(response.toString());
                final String success=jsonObjResp.getString("success");

                if(success.equalsIgnoreCase("true"))
                {
                    JSONObject obj=jsonObjResp.getJSONObject("data");
                    String userToken=obj.getString("access_token").toString();

                    sharedPref.setStringPreference(OTPActivity.this, appConstants.userToken_KEY,userToken);
                    sharedPref.setBooleanPreference(OTPActivity.this, appConstants.isLoggedIn,true);
                    appConstants.userToken = userToken;




                    Intent intent = new Intent(OTPActivity.this, MainScreenActivity.class);
                    startActivity(intent);

                    finish();
                }
                else
                {
                    stopSpinWheel();
                    Toast.makeText(OTPActivity.this, "Failed", Toast.LENGTH_LONG).show();
                }

            }
            catch (Exception xx){}
        }
        else if(apiName.equalsIgnoreCase("resend"))
        {
            try
            {
                appConstants.stopSpinWheel();

                JSONObject jsonObjResp = new JSONObject(response.toString());
                final String success=jsonObjResp.getString("success");

                if(success.equalsIgnoreCase("true"))
                {
                    JSONObject obj=jsonObjResp.getJSONObject("data");
                    String userID=obj.getString("userID").toString();
                    String otp=obj.getString("otp").toString();
                    String status=obj.getString("status").toString();

                    appConstants.userID =userID;
                    appConstants.otp =otp;
                    sharedPref.setStringPreference(OTPActivity.this, appConstants.userID_KEY,userID);


                }
                else
                {
                    stopSpinWheel();
                    Toast.makeText(OTPActivity.this, "Login Failed".toString(), Toast.LENGTH_LONG).show();
                }

            }
            catch (Exception xx){}
        }

    }

    @Override
    public void volleyError(String msg, String apiName) {

    }
}
