package com.firefly.android.app.models;

/**
 * Created by HP on 02/10/2017.
 */

public class FAQModel
{
    private String id;
    private String question;
    private String answer;

    public FAQModel(String id, String question, String answer) {
        this.setId(id);
        this.setQuestion(question);
        this.setAnswer(answer);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
