package com.firefly.android.app.fragment;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.adapters.FAQAdapter;
import com.firefly.android.app.adapters.MainMenuAdapter;
import com.firefly.android.app.models.FAQModel;
import com.firefly.android.app.models.MainMenuModel;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FAQFragment extends Fragment {


    RecyclerView faqLV;

    ArrayList<FAQModel> arrayList;
    FAQModel model;
    FAQAdapter adapter;

    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v=inflater.inflate(R.layout.faq_fragment_layout, container, false);

        faqLV=v.findViewById(R.id.faqLV);


        MainScreenActivity.toolbar_title.setText(R.string.faq_dr);


        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getFaqURL;
                    getFAQReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();

        return v;


    }

    private void getFAQReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    arrayList=new ArrayList<>();

                                    JSONObject jsonObjResp = new JSONObject(response.toString());
                                    final String status=jsonObjResp.getString("success");

                                    if(status.equalsIgnoreCase("true"))
                                    {
                                        JSONArray arr=jsonObjResp.getJSONArray("data");
                                        for (int i = 0; i < arr.length(); i++)
                                        {
                                            JSONObject obj=arr.getJSONObject(i);
                                            String id = obj.getString("id");
                                            String quest = obj.getString("question");
                                            String answer =obj.getString("answer");

                                            model = new FAQModel(id, quest, answer);
                                            arrayList.add(model);


                                        }

                                        adapter = new FAQAdapter(getActivity(),arrayList);
                                        faqLV.setAdapter(adapter);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                        faqLV.setLayoutManager(layoutManager);
                                    }

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String bearer = "Bearer ".concat(appConstants.userToken);
                    Map<String, String> headersSys = super.getHeaders();
                    Map<String, String> headers = new HashMap<String, String>();
                    headersSys.remove("Authorization");
                    headers.put("Authorization", bearer);
                    headers.put("language", appConstants.current_language);
                    headers.putAll(headersSys);
                    return headers;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    public static void goToFAQDetails(String quest,String answer)
    {
        FAQDetailsFragment.question=quest;
        FAQDetailsFragment.answer=answer;

        FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main_container, new FAQDetailsFragment());
        tx.addToBackStack(null);
        tx.commit();


    }




}
