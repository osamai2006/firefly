package com.firefly.android.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.adapters.FAQAdapter;
import com.firefly.android.app.adapters.PointsAdapter;
import com.firefly.android.app.models.FAQModel;
import com.firefly.android.app.models.PointsModel;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PointsFragment extends Fragment {


    RecyclerView pointsLV;
    TextView balanceTXT;

    ArrayList<PointsModel> arrayList;
    PointsModel model;
    PointsAdapter adapter;

    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v=inflater.inflate(R.layout.points_fragment_layout, container, false);
        balanceTXT=v.findViewById(R.id.balanceTXT);

        pointsLV=v.findViewById(R.id.pointsV);


        MainScreenActivity.toolbar_title.setText(R.string.points_tab);


        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url =urlClass.getPointsURL;
                    getPointsReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();

        return v;


    }

    private void getPointsReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    arrayList=new ArrayList<>();

                                    JSONObject jsonObjResp = new JSONObject(response.toString());
                                    final String status=jsonObjResp.getString("success");

                                    if(status.equalsIgnoreCase("true"))
                                    {
                                        JSONObject mainObjc=jsonObjResp.getJSONObject("data");

                                        String balance=mainObjc.getString("balance");
                                        balanceTXT.setText(balance);

                                        JSONArray arr=mainObjc.getJSONArray("points");
                                        for (int i = 0; i < arr.length(); i++)
                                        {
                                            JSONObject obj=arr.getJSONObject(i);
                                            String points = obj.getString("points");
                                            String date = obj.getString("createdAt");
                                            model = new PointsModel(points, balance,date);
                                            arrayList.add(model);


                                        }

                                        adapter = new PointsAdapter(getActivity(),arrayList);
                                        pointsLV.setAdapter(adapter);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                        pointsLV.setLayoutManager(layoutManager);
                                    }

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String bearer = "Bearer ".concat(appConstants.userToken);
                    Map<String, String> headersSys = super.getHeaders();
                    Map<String, String> headers = new HashMap<String, String>();
                    headersSys.remove("Authorization");
                    headers.put("Authorization", bearer);
                    headers.put("language", appConstants.current_language);
                    headers.putAll(headersSys);
                    return headers;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }






}
