package com.firefly.android.app.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.activity.RegisterDetailsActivity;
import com.firefly.android.app.activity.SplashActivity;
import com.firefly.android.app.models.CountryModel;
import com.firefly.android.app.others.AndroidMultiPartEntity;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;


import org.apache.http.*;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

/**
 * Created by HP on 5/9/2017.
 */

public class MyProfileFragment extends Fragment  {

    View view;
    EditText nameTXT;
    Spinner profilecountrySpinner;
    Button updateBTN;
    SharedPrefsUtils sharedPref;
    String userID="";
    String notif="0";

    private Uri filePath;
    private Uri fileUri;
    Bitmap bitmap;
    long totalSize = 0;
    String ImageNameFromServer = "";
    String baseUrlFromServer = "";
    String urlFromServer = "";

    ArrayList<CountryModel> countryArrayList;
    CountryModel countryModel;
    String [] countryArr;
    //RadioButton femalRB,maleRB;
    int gender=0;
    Switch notificationSwitch;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.my_profile_layout, container, false);

        userID = sharedPref.getStringPreference(getActivity(), appConstants.userID_KEY);

        nameTXT=view.findViewById(R.id.profile_name_txt);
        profilecountrySpinner=view.findViewById(R.id.profilecountrySpinner);
        updateBTN =view.findViewById(R.id.submitProfBTN);
        notificationSwitch=view.findViewById(R.id.notificationSwitch);
//        femalRB=view.findViewById(R.id.femaleRB);
//        maleRB=view.findViewById(R.id.maleRB);


        MainScreenActivity.toolbar_title.setText(R.string.my_account_txt);

        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                {
                    notif="1";
                }
                else
                {
                    notif="0";
                }
            }
        });


//        profile_image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showFileChooser();
//            }
//        });

//     maleRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//         @Override
//         public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//             if(isChecked)
//                 gender=1;
//                         else
//                             gender=2;
//         }
//     });
//
//
//        femalRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                if(isChecked)
//                    gender=2;
//                else
//                    gender=1;
//            }
//        });




        updateBTN.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(checkRule()) {
                    appConstants.startSpinwheel(getActivity(), false, true);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {

                                String token=sharedPref.getStringPreference(getActivity(),appConstants.mobileToken_KEY);

                                JSONObject objct = new JSONObject();
                                objct.putOpt("name", nameTXT.getText().toString().trim());
                                //objct.putOpt("email", emailTXT.getText().toString().trim());
                                objct.putOpt("notifications", notif);
                                objct.putOpt("country",Integer.parseInt(countryArrayList.get(profilecountrySpinner.getSelectedItemPosition()).getId()));
                                objct.putOpt("deviceToken",token);
                                objct.putOpt("gender",gender);
//                                objct.putOpt("path",ImageNameFromServer);
//                                objct.putOpt("baseUrl",baseUrlFromServer);
//                                objct.putOpt("url",urlFromServer);
//                                if(!profilPassTXT.getText().toString().trim().equalsIgnoreCase(""))
//                                {
//                                    objct.putOpt("password", profilPassTXT.getText().toString().trim());
//                                }

                                postUpdateProfileRequest(urlClass.updateProfileURL+"update", objct);

                            } catch (Exception xx) {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }
                        }
                    }).start();
                }
            }
        });




        appConstants.startSpinwheel(getActivity(),false,true);
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    getProfileRequest(urlClass.getProfileURL+"profile", null);

                } catch (Exception xx) {
                    xx.toString();
                    appConstants.stopSpinWheel();
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getCountryURL;
                    getCountriestReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();



        return view;
    }
    private  boolean checkRule()
    {
        if (nameTXT.getText().toString().trim().equals(""))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }




        return true;


    }

    private void postUpdateProfileRequest(String urlPost, final JSONObject jsonObjectRequest) {

        appConstants.startSpinwheel(getActivity(),false,true);
        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            //Toast.makeText(getActivity(), "Success", Toast.LENGTH_LONG).show();
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                        else
                        {
                            Toast.makeText(getActivity(),"Failed", Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    private void getProfileRequest(String urlPost, final JSONObject jsonObjectRequest) {


        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true")) {
                            JSONObject obj = jsonObjResp.getJSONObject("data");
                            profilecountrySpinner.setSelection(0);
                            nameTXT.setText(obj.getString("name"));

                            if (obj.getString("notifications").equalsIgnoreCase("0"))
                            {
                                notificationSwitch.setChecked(false);
                            }
                            else
                            {
                                notificationSwitch.setChecked(true);
                            }

                            sharedPref.setStringPreference(getActivity(), appConstants.notificationStatus_KEY,obj.getString("notifications"));
                            //appConstants.profilePicture = obj.getString("profilePicture");
//                            if (obj.getString("gender").equalsIgnoreCase("1"))
//                            {
//                                maleRB.setChecked(true);
//                            }
//                            else
//                            {
//                                femalRB.setChecked(true);
//                            }

//                            Glide.with(getActivity()).load(appConstants.profilePicture)
//                                    .apply(RequestOptions.placeholderOf(R.drawable.prof_img))
//                                    .apply(RequestOptions.circleCropTransform()).into(profile_image);


                        }




                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }


//
        }

        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    private void getCountriestReq(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        countryArrayList=new ArrayList<>();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONArray mainArr=jsonObjResp.getJSONArray("data");
                            countryArr=new String[mainArr.length()];

                            for (int i=0;i<mainArr.length();i++)
                            {

                                JSONObject obj=mainArr.getJSONObject(i);

                                String id=obj.getString("id");
                                String name=obj.getString("name");
                                String currency=obj.getString("currency");
                                String tax=obj.getString("tax");
                                String code=obj.getString("countryCode");
                                String phoneCode=obj.getString("phoneCode");

                                countryModel=new CountryModel(id,name,currency,tax,code,phoneCode);
                                countryArrayList.add(countryModel);
                                countryArr[i]=name;

                            }


                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, countryArr);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            profilecountrySpinner.setAdapter(spinnerArrayAdapter);


                        }


                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Your Image"), 1111);

    }

    public Uri getOutputMediaFileUri(int type)
    {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), appConstants.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs())
            {
                return null;

            }

        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1111 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
                //Glide.with(this).load(stream.toByteArray()).apply(RequestOptions.circleCropTransform()).into(profile_image);

                new UploadFileToServer().execute();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public  String getRealPathFromDocumentUri( Uri uri){
        String filePath = "";

        try {


            Pattern p = Pattern.compile("(\\d+)$");
            Matcher m = p.matcher(uri.toString());
            if (!m.find()) {
                return filePath;
            }
            String imgId = m.group();

            String[] column = {MediaStore.Images.Media.DATA};
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = getActivity().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{imgId}, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        catch (Exception xx){}

        return filePath;
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {

            appConstants.startSpinwheel(getActivity(),false,true);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile()
        {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urlClass.uploadPhotoURL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });


                File sourceFile = new File(getRealPathFromDocumentUri(filePath));

                // Adding file data to http body
                entity.addPart("profilePicture", new FileBody(sourceFile));

                // Extra parameters if you want to pass to server
//                entity.addPart("website",new StringBody("http://api.burgerfirefly.com"));
//                entity.addPart("email",new StringBody("osamai2006@gmail.com"));

                totalSize = entity.getContentLength();
                httppost.addHeader("Authorization","Bearer "+appConstants.userToken);
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200)
                {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                }
                else
                {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            try
            {
                appConstants.stopSpinWheel();
                JSONObject jsonObjResp = new JSONObject(result.toString());
                final String status=jsonObjResp.getString("success");

                if(status.equalsIgnoreCase("true"))
                {
                    JSONObject mainObj=jsonObjResp.getJSONObject("data");
                    ImageNameFromServer =mainObj.getString("path");
                    baseUrlFromServer =mainObj.getString("baseUrl");
                    urlFromServer =mainObj.getString("url");
                }
                else
                {

                }


            }
            catch (JSONException e) {
                e.printStackTrace();
            }


            super.onPostExecute(result);
        }


    }

}
