package com.firefly.android.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.adapters.FAQAdapter;
import com.firefly.android.app.adapters.OrdersAdapter;
import com.firefly.android.app.models.FAQModel;
import com.firefly.android.app.models.OrdersModel;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyOrderFragment extends Fragment {


    static RecyclerView orderLV;

    static ArrayList<OrdersModel> arrayList;
    static OrdersModel model;
    static OrdersAdapter adapter;

    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v=inflater.inflate(R.layout.my_orders_fragment_layout, container, false);

        orderLV=v.findViewById(R.id.orderLV);


        MainScreenActivity.toolbar_title.setText(R.string.orders_dr);


        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url =urlClass.getMyOrderURL;
                    getOrdersReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();

        return v;


    }

    private static void getOrdersReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(myContext);

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    arrayList=new ArrayList<>();

                                    JSONObject jsonObjResp = new JSONObject(response.toString());
                                    final String status=jsonObjResp.getString("success");

                                    if(status.equalsIgnoreCase("true"))
                                    {
                                        JSONArray arr=jsonObjResp.getJSONArray("data");
                                        for (int i = 0; i < arr.length(); i++)
                                        {
                                            JSONObject obj=arr.getJSONObject(i);

                                            String id = obj.getString("id");
                                            String number = obj.getString("orderNumber");
                                            String date =obj.getString("deliveryDate");
                                            String time =obj.getString("orderTime");
                                            String type =obj.getString("orderType");
                                            String price =obj.getString("deliveryPrice");
                                            String tax =obj.getString("tax");
                                            String total =obj.getString("orderTotal");

                                            JSONArray itemsArr=new JSONArray();

                                            String qty ="0";
                                            try{itemsArr=obj.getJSONArray("items");}catch (Exception xx){}
                                            try
                                            {
                                                JSONObject itemObj=itemsArr.getJSONObject(i);
                                                qty=itemObj.getString("quantity");
                                            }
                                            catch (Exception xx){}

                                            model = new OrdersModel(id, number, date,time,type,price,tax,total,qty,itemsArr);
                                            arrayList.add(model);


                                        }

                                        adapter = new OrdersAdapter(myContext,arrayList);
                                        orderLV.setAdapter(adapter);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(myContext, LinearLayoutManager.VERTICAL, false);
                                        orderLV.setLayoutManager(layoutManager);

                                    }

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    Toast.makeText(myContext,myContext.getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String bearer = "Bearer ".concat(appConstants.userToken);
                    Map<String, String> headersSys = super.getHeaders();
                    Map<String, String> headers = new HashMap<String, String>();
                    headersSys.remove("Authorization");
                    headers.put("Authorization", bearer);
                    headers.putAll(headersSys);
                    return headers;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    public static void reOrderReq(final String itemID, final int pos) {

        appConstants.startSpinwheel(myContext, false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonObjectRequest jsonObjReq;
                JSONObject obj=new JSONObject();
                try
                {
                    JSONArray noteArr=arrayList.get(pos).getItemsArr();
                    JSONObject noteObj=noteArr.getJSONObject(0);
                    String note=noteObj.getString("note");

                    obj.putOpt("orderId",itemID);
                    obj.putOpt("deliveryTime",arrayList.get(pos).getTime());
                    obj.putOpt("deliveryMethod",arrayList.get(pos).getType());
                    obj.putOpt("note",note);

                }
                catch (Exception xx){}

                jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlClass.reOrderURL, obj, new Response.Listener<JSONObject>()
                {

                    @Override
                    public void onResponse(JSONObject response) {

                        try
                        {
                            if(response !=null)
                            {
                                appConstants.stopSpinWheel();

                                JSONObject jsonObjResp = new JSONObject(response.toString());
                                final String status=jsonObjResp.getString("success");

                                if(status.equalsIgnoreCase("true"))
                                {
                                    Toast.makeText(myContext, "Success", Toast.LENGTH_LONG).show();
                                    appConstants.startSpinwheel(myContext, false, true);

                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {

                                            try {

                                                String url =urlClass.getMyOrderURL;
                                                getOrdersReq(url, null);

                                            } catch (Exception xx)
                                            {
                                                String xxx=xx.toString();
                                            }
                                        }
                                    }).start();

                                }


                            }
                            else
                            {
                                appConstants.stopSpinWheel();
                                Toast.makeText(myContext, myContext.getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            }


                        }
                        catch (Exception e)
                        {
                            Toast.makeText(myContext,myContext.getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            appConstants.stopSpinWheel();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Toast.makeText(myContext,myContext.getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                        appConstants.stopSpinWheel();

                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        String bearer = "Bearer ".concat(appConstants.userToken);
                        Map<String, String> headersSys = super.getHeaders();
                        Map<String, String> headers = new HashMap<String, String>();
                        headersSys.remove("Authorization");
                        headers.put("Authorization", bearer);
                        headers.put("language", appConstants.current_language);
                        headers.putAll(headersSys);
                        return headers;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(myContext);
                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                        appConstants.appTimeOut,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(jsonObjReq);
            }
        }).start();

    }

    public static void goToOrderReq(final String itemID, final int pos) {

        InvoiceFragment.orderID=itemID;
        InvoiceFragment.invoiceFlag="2";
        InvoiceFragment.position=pos;


        myContext.getSupportFragmentManager().popBackStack();

        FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main_container, new InvoiceFragment());
        tx.addToBackStack(null);
        tx.commit();

    }




}
