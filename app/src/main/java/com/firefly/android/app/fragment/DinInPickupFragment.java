package com.firefly.android.app.fragment;

import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.activity.RegisterDetailsActivity;
import com.firefly.android.app.models.BranchModel;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class DinInPickupFragment extends Fragment {


   Spinner branchSspin;

    static ArrayList<BranchModel> branchArrayList;
    BranchModel branchModel;
    public static String [] branchArr;
    Button submitOrderBTN;
    EditText nameTxt,mobileTxt,timeTXT,notesTXT;
    public static String checkoutMethod="";
    SharedPrefsUtils sharedPref;
    String mobileNum="";


    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v=inflater.inflate(R.layout.dinin_pickup_layout, container, false);
        if(checkoutMethod.equalsIgnoreCase("2"))
        {

            MainScreenActivity.toolbar_title.setText(R.string.dinin);
        }
        else
        {

            MainScreenActivity.toolbar_title.setText(R.string.takeaway);
        }

        mobileNum = sharedPref.getStringPreference(getActivity(), appConstants.userName_KEY);
        branchSspin=v.findViewById(R.id.branchSspin);
        submitOrderBTN=v.findViewById(R.id.submitOrderBTN);
        nameTxt=v.findViewById(R.id.nameTxt);
        mobileTxt=v.findViewById(R.id.mobileTxt);
        mobileTxt.setText(mobileNum);
        timeTXT=v.findViewById(R.id.timeTXT);
        notesTXT=v.findViewById(R.id.notesTXT);

        timeTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                setTimePickerDialog();
            }
        });

        submitOrderBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkRule())
                {

                    ConfirmationFragmentNew.checkoutMethod=checkoutMethod;
                    ConfirmationFragmentNew.time=timeTXT.getText().toString().trim();
                    ConfirmationFragmentNew.notes=notesTXT.getText().toString().trim();
                    ConfirmationFragmentNew.branchID=Integer.parseInt(branchArrayList.get(branchSspin.getSelectedItemPosition()).getId());
                    ConfirmationFragmentNew.branchName=branchArrayList.get(branchSspin.getSelectedItemPosition()).getName();
                    ConfirmationFragmentNew.deleveryPrice="";

                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new ConfirmationFragmentNew());
                    tx.addToBackStack(null);
                    tx.commit();

//                    appConstants.startSpinwheel(getActivity(), false, true);
//
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run()
//                        {
//
//                            try
//                            {
//
//                                String url= urlClass.checkOutDininPickupURL;
//
//                                JSONObject objct = new JSONObject();
//
//                                //objct.putOpt("name", nameTxt.getText().toString().trim());
//                                objct.putOpt("deliveryTime", timeTXT.getText().toString().trim());
//                               // objct.putOpt("phone", mobileTxt.getText().toString().trim());
//                                objct.putOpt("note",notesTXT.getText().toString().trim());
//                                objct.putOpt("branchId",Integer.parseInt(branchArrayList.get(branchSspin.getSelectedItemPosition()).getId()));
//                                objct.putOpt("deliveryMethod",checkoutMethod);
//
//                                checkOutRequest(url,objct);
//                            }
//                            catch (Exception xx)
//                            {
//                                xx.toString();
//                                appConstants.stopSpinWheel();
//                            }
//                        }
//                    }).start();
                }

            }
        });


        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getBranchsURL;
                    getBranchestReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();



        return v;


    }


    private  boolean checkRule()
    {
//        if (nameTxt.getText().toString().trim().equals("")) {
//            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
//            return false;
//        }
//
//
//        if (mobileTxt.getText().toString().trim().equals("")) {
//            Toast.makeText(getContext(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
//            return false;
//        }



        if (timeTXT.getText().toString().trim().equals(""))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }


        return  true;
    }



    private void checkOutRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {

                            JSONObject obj=jsonObjResp.getJSONObject("data");
                            String id=obj.getString("id");
                            String name=obj.getString("name");
                            String phone=obj.getString("phone");
                            String orderNum=obj.getString("orderNumber");
                            String orderDate=obj.getString("orderDate");
                            String orderTime=obj.getString("orderTime");
                            String orderType=obj.getString("orderType");
                            String deliveryPrice=obj.getString("deliveryPrice");
                            String tax=obj.getString("tax");
                            String orderTotal=obj.getString("orderTotal");

                            try
                            {
                                JSONObject objct = new JSONObject();

                                objct.putOpt("orderId", id);
                                objct.putOpt("accepted", "1");

                                placeOrderRequest(urlClass.placeOrderURL,objct);
                            }
                            catch (Exception xx){}




                        }
                        else
                        {
                            final String status_message=jsonObjResp.getString("status_message");
                            Toast.makeText(getActivity(), status_message, Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    private void placeOrderRequest(final String urlPost, final JSONObject jsonObjectRequest) {

        appConstants.startSpinwheel(getActivity(),false,true);
        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonObjectRequest jsonObjReq;

                jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
                {

                    @Override
                    public void onResponse(JSONObject response) {

                        try
                        {
                            if(response !=null)
                            {
                                appConstants.stopSpinWheel();

                                JSONObject jsonObjResp = new JSONObject(response.toString());
                                final String status=jsonObjResp.getString("success");

                                if(status.equalsIgnoreCase("true"))
                                {

                                    JSONObject obj=jsonObjResp.getJSONObject("data");

                                    String orderId=obj.getString("orderId");

                                    InvoiceFragment.orderID=orderId;
                                    InvoiceFragment.invoiceFlag="1";

                                    myContext.getSupportFragmentManager().popBackStack();

                                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                                    tx.replace(R.id.main_container, new InvoiceFragment());
                                   // tx.addToBackStack(null);
                                    tx.commit();





                                }
                                else
                                {
                                    final String status_message=jsonObjResp.getString("success");
                                    Toast.makeText(getActivity(), status_message, Toast.LENGTH_LONG).show();
                                }

                            }
                            else
                            {
                                appConstants.stopSpinWheel();
                                Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            }


                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            appConstants.stopSpinWheel();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                        appConstants.stopSpinWheel();

                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        String bearer = "Bearer ".concat(appConstants.userToken);
                        Map<String, String> headersSys = super.getHeaders();
                        Map<String, String> headers = new HashMap<String, String>();
                        headersSys.remove("Authorization");
                        headers.put("Authorization", bearer);
                        headers.putAll(headersSys);
                        return headers;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                        appConstants.appTimeOut,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(jsonObjReq);


            }
        }).start();

    }

    private void getBranchestReq(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        branchArrayList=new ArrayList<>();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONArray mainArr=jsonObjResp.getJSONArray("data");
                            branchArr=new String[mainArr.length()];

                            for (int i=0;i<mainArr.length();i++)
                            {

                                JSONObject obj=mainArr.getJSONObject(i);

                                String id=obj.getString("id");
                                String name=obj.getString("name");
                                String country=obj.getString("country");
                                String city=obj.getString("city");
                                String phone=obj.getString("phone");
                                String address=obj.getString("address");
                                String longt=obj.getString("longitude");
                                String latitu=obj.getString("latitude");
                                JSONArray regionArrJson=new JSONArray();
                                regionArrJson=obj.getJSONArray("regions");

                                branchModel=new BranchModel(id,name,country,city,phone,address,longt,latitu,regionArrJson);
                                branchArrayList.add(branchModel);
                                branchArr[i]=name;

                            }


                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, branchArr);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            branchSspin.setAdapter(spinnerArrayAdapter);



                        }


                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }


    private void setTimePickerDialog()
    {
        final Calendar mcurrentTime = Calendar.getInstance();
        final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);


        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute)
                    {
                        timeTXT.setText(hourOfDay + ":" + minute);
                    }
                }, hour, minute, true);

        timePickerDialog.show();

//        TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
//            @Override
//            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//                if (view.isShown()) {
//                    mcurrentTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
//                    mcurrentTime.set(Calendar.MINUTE, minute);
//                    startTimeTXT.setText(hourOfDay + ":" + minute);
//
//                }
//            }
//        };
//
//        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
//        timePickerDialog.setTitle("Start Time:");
//        timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        timePickerDialog.show();


    }



}
