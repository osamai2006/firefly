package com.firefly.android.app.models;

/**
 * Created by HP on 3/25/2018.
 */

public class RegionsModel {

    private String id;
    private String name;
    private String deliveryPrice;
    private String regionHash;

    public RegionsModel(String id, String name, String deliveryPrice, String regionHash) {
        this.setId(id);
        this.setName(name);
        this.setDeliveryPrice(deliveryPrice);
        this.setRegionHash(regionHash);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeliveryPrice() {
        return deliveryPrice;
    }

    public void setDeliveryPrice(String deliveryPrice) {
        this.deliveryPrice = deliveryPrice;
    }

    public String getRegionHash() {
        return regionHash;
    }

    public void setRegionHash(String regionHash) {
        this.regionHash = regionHash;
    }
}
