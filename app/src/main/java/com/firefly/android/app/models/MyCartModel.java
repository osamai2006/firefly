package com.firefly.android.app.models;

import org.json.JSONArray;

public class MyCartModel {

    private String id;
    private String cartItemId;
    private String image;
    private String name;
    private String price;
    private String quentity;
    private JSONArray ingrediantArr;
    private JSONArray extrasArr;
    private String itemType;

    public MyCartModel(String id, String cartItemId, String image, String name, String price, String quentity, JSONArray ingrediantArr, JSONArray extrasArr, String itemType) {
        this.id = id;
        this.cartItemId = cartItemId;
        this.image = image;
        this.name = name;
        this.price = price;
        this.quentity = quentity;
        this.ingrediantArr = ingrediantArr;
        this.extrasArr = extrasArr;
        this.itemType = itemType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCartItemId() {
        return cartItemId;
    }

    public void setCartItemId(String cartItemId) {
        this.cartItemId = cartItemId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuentity() {
        return quentity;
    }

    public void setQuentity(String quentity) {
        this.quentity = quentity;
    }

    public JSONArray getIngrediantArr() {
        return ingrediantArr;
    }

    public void setIngrediantArr(JSONArray ingrediantArr) {
        this.ingrediantArr = ingrediantArr;
    }

    public JSONArray getExtrasArr() {
        return extrasArr;
    }

    public void setExtrasArr(JSONArray extrasArr) {
        this.extrasArr = extrasArr;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }
}
