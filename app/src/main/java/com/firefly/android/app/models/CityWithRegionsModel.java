package com.firefly.android.app.models;

import org.json.JSONArray;

public class CityWithRegionsModel {

    private String id;
    private String country;
    private String name;
    private String code;
    private JSONArray regions;

    public CityWithRegionsModel(String id, String country, String name, String code, JSONArray regions) {
        this.id = id;
        this.country = country;
        this.name = name;
        this.code = code;
        this.regions = regions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public JSONArray getRegions() {
        return regions;
    }

    public void setRegions(JSONArray regions) {
        this.regions = regions;
    }
}
