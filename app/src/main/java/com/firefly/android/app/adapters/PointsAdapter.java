package com.firefly.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firefly.android.app.R;
import com.firefly.android.app.fragment.FAQFragment;
import com.firefly.android.app.models.FAQModel;
import com.firefly.android.app.models.PointsModel;
import com.firefly.android.app.others.appConstants;

import java.util.ArrayList;


/**
 * Created by HP on 8/27/2017.
 */

public class PointsAdapter extends RecyclerView.Adapter<PointsAdapter.MyViewHolder> {



    Context context;


    ArrayList<PointsModel> arrayList;

    //int pos=0;
    // ProgressDialog progressBarNew;
    public PointsAdapter(Context context, ArrayList<PointsModel> modelArray) {

        this.context = context;
        this.arrayList = modelArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.points_grid_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.pointsTXT.setText("You got "+arrayList.get(position).getPoints()+" Points");
       try{ holder.dateTXT.setText(appConstants.getDate(Long.parseLong( arrayList.get(position).getDate())));}
       catch (Exception xx){}


    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView pointsTXT,dateTXT;

        public MyViewHolder(View view) {
            super(view);

            pointsTXT = (TextView) view.findViewById(R.id.pointsTXT);
            dateTXT= (TextView) view.findViewById(R.id.dateTXT);



        }
    }




    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

}