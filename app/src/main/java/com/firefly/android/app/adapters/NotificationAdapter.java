package com.firefly.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firefly.android.app.R;
import com.firefly.android.app.models.NotificationModel;

import java.util.ArrayList;

/**
 * Created by Ayadi on 2/6/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MenuItemViewHolder> {

    Context context;
    ArrayList<NotificationModel> resourcelList;



    public NotificationAdapter(Context context, ArrayList<NotificationModel> ResourcelList) {
        this.context = context;
        resourcelList = ResourcelList;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item_row, parent, false);
        MenuItemViewHolder viewholder = new MenuItemViewHolder(view);
        return viewholder;
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder holder, final int position) {

        holder.titleTXT.setText(resourcelList.get(position).getTitle());
        holder.descTXT.setText(resourcelList.get(position).getBody());
       // Glide.with(context).load( resourcelList.get(position).getImage()).apply(RequestOptions.circleCropTransform()).into(holder.image);



    }

    @Override
    public int getItemCount() {
        return resourcelList.size();
    }

    public static class MenuItemViewHolder extends RecyclerView.ViewHolder
    {

        TextView titleTXT,descTXT ;
        ImageView image;


        public MenuItemViewHolder(View itemView)
        {
            super(itemView);

            titleTXT = itemView.findViewById(R.id.titleTXT);
            descTXT= itemView.findViewById(R.id.descTXT);
            image= itemView.findViewById(R.id.image);


        }


    }




}
