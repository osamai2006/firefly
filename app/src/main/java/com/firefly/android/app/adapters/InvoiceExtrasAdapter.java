package com.firefly.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firefly.android.app.R;
import com.firefly.android.app.fragment.MyCartFragment;
import com.firefly.android.app.models.ExtrasModel;
import com.firefly.android.app.models.MyCartModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by HP on 8/27/2017.
 */

public class InvoiceExtrasAdapter extends RecyclerView.Adapter<InvoiceExtrasAdapter.MyViewHolder> {



    Context context;
    ArrayList<ExtrasModel> arrayList;


    public InvoiceExtrasAdapter(Context context, ArrayList<ExtrasModel> modelArray) {

        this.context = context;
        this.arrayList = modelArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.invoice_extras_grid_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.nameTXT.setText(arrayList.get(position).getName());
        holder.priceTXT.setText(arrayList.get(position).getPrice() +" JD");

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTXT,priceTXT;

        public MyViewHolder(View view) {
            super(view);

            nameTXT = (TextView) view.findViewById(R.id.nameTXT);
            priceTXT = (TextView) view.findViewById(R.id.priceTXT);

        }
    }




    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

}