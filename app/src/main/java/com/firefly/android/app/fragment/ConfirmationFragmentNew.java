package com.firefly.android.app.fragment;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ConfirmationFragmentNew extends Fragment{



    TextView nameTXT,mobileTXT,priceTXT,branchTXT,regionTXT,usedPointsTXT,earnPointsTXT,deliveryPriceTXT;
    Button confirmBTN,cancelBTN;

    SharedPrefsUtils sharedPref;
    public static String mobileNum="",nickName="",price="",branchName="----------",region="",checkoutMethod="",
                        deliveryAddressName="",deliveryBuildingNum="",time="",notes="",deleveryPrice="";
    public static int  branchID;


    public static JSONObject addressObj=new JSONObject();




    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v=inflater.inflate(R.layout.confirmation_fragment_layout, container, false);


        MainScreenActivity.toolbar_title.setText(R.string.confirm_cart);


        mobileNum = sharedPref.getStringPreference(getActivity(), appConstants.userName_KEY);
        nickName = sharedPref.getStringPreference(getActivity(), appConstants.userNickName);
        region = sharedPref.getStringPreference(getActivity(), appConstants.userCountry_KEY);

        confirmBTN=v.findViewById(R.id.confirmBTN);
        cancelBTN=v.findViewById(R.id.cancelBTN);

        nameTXT=v.findViewById(R.id.nameTXT);
        mobileTXT = v.findViewById(R.id.mobileTXT);
        priceTXT =  v.findViewById(R.id.priceTXT);
        branchTXT =  v.findViewById(R.id.branchTXT);
        regionTXT=  v.findViewById(R.id.regionTXT);
        usedPointsTXT=  v.findViewById(R.id.usedPointsTXT);
        earnPointsTXT=  v.findViewById(R.id.earnPointsTXT);
        deliveryPriceTXT=  v.findViewById(R.id.deliveryPriceTXT);

        nameTXT.setText(nickName);
        mobileTXT.setText(mobileNum);
        priceTXT.setText(price);
        regionTXT.setText(region);
        branchTXT.setText(branchName);
        deliveryPriceTXT.setText(deleveryPrice);
        usedPointsTXT.setText("10 Points");
        earnPointsTXT.setText("10 Points");


        confirmBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    appConstants.startSpinwheel(getActivity(), false, true);

                    new Thread(new Runnable() {
                        @Override
                        public void run()
                        {

                            try
                            {
                                String url="";
                                JSONObject objct = new JSONObject();

                                if(checkoutMethod.equalsIgnoreCase("1"))//delivery
                                {
                                     url= urlClass.deleverycheckOutURL;

                                    JSONObject geoObj = new JSONObject();

                                    addressObj.putOpt("title",deliveryAddressName);
                                    addressObj.putOpt("buildingNumber",deliveryBuildingNum);
                                    geoObj.putOpt("latitude",appConstants.latit);
                                    geoObj.putOpt("longitude",appConstants.longt);
                                    addressObj.putOpt("geo",geoObj);

                                    objct.putOpt("deliveryMethod","1");
                                    objct.putOpt("address",addressObj);
                                }
                                else if(checkoutMethod.equalsIgnoreCase("2") ||
                                        checkoutMethod.equalsIgnoreCase("3"))//din in and pickup
                                {
                                     url= urlClass.checkOutDininPickupURL;

                                    objct.putOpt("deliveryTime", time);
                                    objct.putOpt("note",notes);
                                    objct.putOpt("branchId",branchID);
                                    objct.putOpt("deliveryMethod",checkoutMethod);
                                }


                                checkOutRequest(url,objct);


                            }
                            catch (Exception xx)
                            {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }
                        }
                    }).start();


            }
        });

        cancelBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getSupportFragmentManager().popBackStack();
            }
        });


        return v;


    }





    private void checkOutRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {

                            JSONObject obj=jsonObjResp.getJSONObject("data");
                            String id=obj.getString("id");
                            String name=obj.getString("name");
                            String phone=obj.getString("phone");
                            String orderNum=obj.getString("orderNumber");
                            String orderDate=obj.getString("orderDate");
                            String orderTime=obj.getString("orderTime");
                            String orderType=obj.getString("orderType");
                            String deliveryPrice=obj.getString("deliveryPrice");
                            String tax=obj.getString("tax");
                            String orderTotal=obj.getString("orderTotal");

                            try
                            {
                                JSONObject objct = new JSONObject();

                                objct.putOpt("orderId", id);
                                objct.putOpt("accepted", "1");

                                placeOrderRequest(urlClass.placeOrderURL,objct);
                            }
                            catch (Exception xx){}




                        }
                        else
                        {
                            final String status_message=jsonObjResp.getString("status_message");
                            Toast.makeText(getActivity(), status_message, Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    private void placeOrderRequest(final String urlPost, final JSONObject jsonObjectRequest) {

        appConstants.startSpinwheel(getActivity(),false,true);
        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonObjectRequest jsonObjReq;

                jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
                {

                    @Override
                    public void onResponse(JSONObject response) {

                        try
                        {
                            if(response !=null)
                            {
                                appConstants.stopSpinWheel();

                                JSONObject jsonObjResp = new JSONObject(response.toString());
                                final String status=jsonObjResp.getString("success");

                                if(status.equalsIgnoreCase("true"))
                                {

                                    JSONObject obj=jsonObjResp.getJSONObject("data");

                                    String orderId=obj.getString("orderId");

                                    InvoiceFragment.orderID=orderId;
                                    InvoiceFragment.invoiceFlag="1";

                                    myContext.getSupportFragmentManager().popBackStack();

                                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                                    tx.replace(R.id.main_container, new InvoiceFragment());
                                    //tx.addToBackStack(null);
                                    tx.commit();





                                }
                                else
                                {
                                    final String status_message=jsonObjResp.getString("success");
                                    Toast.makeText(getActivity(), status_message, Toast.LENGTH_LONG).show();
                                }

                            }
                            else
                            {
                                appConstants.stopSpinWheel();
                                Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            }


                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            appConstants.stopSpinWheel();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                        appConstants.stopSpinWheel();

                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        String bearer = "Bearer ".concat(appConstants.userToken);
                        Map<String, String> headersSys = super.getHeaders();
                        Map<String, String> headers = new HashMap<String, String>();
                        headersSys.remove("Authorization");
                        headers.put("Authorization", bearer);
                        headers.putAll(headersSys);
                        return headers;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                        appConstants.appTimeOut,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(jsonObjReq);


            }
        }).start();

    }


}
