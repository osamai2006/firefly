package com.firefly.android.app.models;

/**
 * Created by HP on 02/10/2017.
 */

public class AdsClass
{
    private String adsID;
    private String adsName;
    private String adsImgURL;

    public AdsClass(String adsID, String adsName, String adsImgURL) {
        this.setAdsID(adsID);
        this.setAdsName(adsName);
        this.setAdsImgURL(adsImgURL);
    }

    public String getAdsID() {
        return adsID;
    }

    public void setAdsID(String adsID) {
        this.adsID = adsID;
    }

    public String getAdsName() {
        return adsName;
    }

    public void setAdsName(String adsName) {
        this.adsName = adsName;
    }

    public String getAdsImgURL() {
        return adsImgURL;
    }

    public void setAdsImgURL(String adsImgURL) {
        this.adsImgURL = adsImgURL;
    }
}
