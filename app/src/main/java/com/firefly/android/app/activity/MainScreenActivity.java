package com.firefly.android.app.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firefly.android.app.R;
import com.firefly.android.app.adapters.MyCartRecycleAdapter;
import com.firefly.android.app.fragment.AboutUsFragment;
import com.firefly.android.app.fragment.AddAddressFragment;
import com.firefly.android.app.fragment.AddressesFragmnet;
import com.firefly.android.app.fragment.ContactUsFragment;
import com.firefly.android.app.fragment.FAQFragment;
import com.firefly.android.app.fragment.MainMenuFragment;
import com.firefly.android.app.fragment.MyCartFragment;
import com.firefly.android.app.fragment.MyOrderFragment;
import com.firefly.android.app.fragment.MyProfileFragment;
import com.firefly.android.app.fragment.NotificationFragment;
import com.firefly.android.app.fragment.PointsFragment;
import com.firefly.android.app.fragment.SettingFragment;
import com.firefly.android.app.fragment.SliderFragment;
import com.firefly.android.app.fragment.WorldMapFragmnet;
import com.firefly.android.app.models.CountryModel;
import com.firefly.android.app.models.MyCartModel;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MainScreenActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    NavigationView navigationView;
    public static ImageView imgmenu,navImageView,cartImg,settingBTN;
    TextView lang_toolbar,profileNameTXT,countryTXT;
    ImageView profile_image;
    public static TextView cartCountTXT,toolbar_title;

    private DrawerLayout drawer;
    private Fragment fragment;
    FragmentTransaction tx;
    SharedPrefsUtils sharedPref;
    boolean isLoged;
    String userName="";
    Button logoutTXT;
    static Dialog dialog;
    String country="";


    public static AppCompatActivity myActivity;


    static ArrayList<MyCartModel> cartArrayList;
    static MyCartModel cartModel;

    @Override
    protected void onResume() {
        try {
            super.onResume();


            if (appConstants.current_language.equalsIgnoreCase("Ar")) {
                lang_toolbar.setText("AR");

            } else {
                lang_toolbar.setText("ENG");
            }

            isLoged = sharedPref.getBooleanPreference(getApplicationContext(), appConstants.isLoggedIn, false);
            country = sharedPref.getStringPreference(getApplicationContext(), appConstants.userCountry_KEY);
            if (isLoged) {
                //userName = sharedPref.getStringPreference(getApplicationContext(), appConstants.userName_KEY);
                profileNameTXT.setVisibility(View.VISIBLE);
                //profileNameTXT.setText(userName);
                logoutTXT.setText("Logout");

//                Glide.with(MainScreenActivity.this).load(appConstants.profilePicture)
//                        .apply(RequestOptions.placeholderOf(R.drawable.prof_img))
//                        .apply(RequestOptions.circleCropTransform()).into(profile_image);

            } else {
                profileNameTXT.setVisibility(View.GONE);
                profileNameTXT.setText("");
                logoutTXT.setText(getResources().getString(R.string.login_txt));

            }

        }
        catch (Exception xx){}

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.main_screen_layout);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       // toolbar.setNavigationIcon(R.color.White);

        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);

        toolbar_title=  findViewById(R.id.toolbar_title);
        toolbar_title.setText(R.string.menu_tab);

        cartCountTXT=  findViewById(R.id.cartCountTXT);

        cartImg=findViewById(R.id.cartImg);
        myActivity=MainScreenActivity.this;
        cartImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isLoged = sharedPref.getBooleanPreference(getApplicationContext(), appConstants.isLoggedIn, false);
                if (isLoged)
                {
                    FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new MyCartFragment());
                    tx.addToBackStack(null);
                    tx.commit();
                }
                else
                {
                    startActivity(new Intent(MainScreenActivity.this,LoginActivity.class));
                }

            }
        });


        //setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();




        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(Color.GRAY);
        drawable.setSize(1, 1);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
//        navigationView.inflateMenu(R.menu.menu_base);
//        assert navigationView != null;

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);



        isLoged = sharedPref.getBooleanPreference(getApplicationContext(), appConstants.isLoggedIn, false);
        appConstants.userToken = sharedPref.getStringPreference(getApplicationContext(), appConstants.userToken_KEY);
        appConstants.userID = sharedPref.getStringPreference(getApplicationContext(), appConstants.userID_KEY);

        navigationView.inflateMenu(R.menu.menu_base);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.inflateHeaderView(R.layout.nav_header);

        View header = navigationView.getHeaderView(0);

        lang_toolbar = (TextView) findViewById(R.id.lang_toolbar);

        navImageView = (ImageView) header.findViewById(R.id.profile_image);
        settingBTN = (ImageView) header.findViewById(R.id.settingBTN);
        countryTXT= (TextView) header.findViewById(R.id.countryTXT);
        logoutTXT= findViewById(R.id.logoutBTN_menu);
        profileNameTXT = (TextView) header.findViewById(R.id.profile_name);
        profile_image= (ImageView) header.findViewById(R.id.profile_image);
        logoutTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(isLoged)
                {
                    appConstants.startSpinwheel(MainScreenActivity.this, false, true);

                    new Thread(new Runnable() {
                        @Override
                        public void run()
                        {

                            try
                            {

                                String url= urlClass.logoutURL;
                                String token=sharedPref.getStringPreference(getApplicationContext(),appConstants.mobileToken_KEY);
                                String password=sharedPref.getStringPreference(getApplicationContext(),appConstants.userPassword_KEY);
                                String phone=sharedPref.getStringPreference(getApplicationContext(),appConstants.userName_KEY);

                                JSONObject registerObjct = new JSONObject();

                                registerObjct.putOpt("password",password);
                                registerObjct.putOpt("phone",phone);
                                registerObjct.putOpt("deviceToken",token);

                                logoutRequest(url,registerObjct);
                            }
                            catch (Exception xx)
                            {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }
                        }
                    }).start();

                }
                else
                {
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);

                }
            }
           });


                if (isLoged)
                {
                    userName=sharedPref.getStringPreference(getApplicationContext(),appConstants.userName_KEY);
                    profileNameTXT.setVisibility(View.VISIBLE);
                    profileNameTXT.setText(userName);
                    country = sharedPref.getStringPreference(getApplicationContext(), appConstants.userCountry_KEY);
                    countryTXT.setText(country);

                }
                else
                {
                    profileNameTXT.setVisibility(View.GONE);
                    profileNameTXT.setText("");
                    logoutTXT.setText(getResources().getString(R.string.login_txt));

                }

        settingBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, new SettingFragment());
                tx.addToBackStack(null);
                tx.commit();

                drawer.closeDrawer(Gravity.LEFT);
            }
        });

//        navImageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                boolean isLoged = sharedPref.getBooleanPreference(getApplicationContext(), appConstants.isLoggedIn, false);
//
//                if (isLoged)
//                {
////                    Intent intent2 = new Intent(MainScreenActivity.this, MyAccountActivity.class);
////                    startActivity(intent2);
//
//                }
//                else
//                {
//                    Intent intent = new Intent(MainScreenActivity.this, LoginActivity.class);
//                    startActivity(intent);
//
//                }
//            }
//        });

        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new SliderFragment());
            // tx.addToBackStack(null);
            tx.commit();



        lang_toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainScreenActivity.this);
                alertDialog.setTitle("Change Language");
                alertDialog.setMessage("Do you want to change language ?");
                //  alertDialog.setIcon(R.drawable.ic_launcher);

                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (lang_toolbar.getText().toString().equalsIgnoreCase("ENG")) {
                            setInitialLanguage("ar");

                        } else {
                            setInitialLanguage("en");

                        }
                    }
                });

                alertDialog.show();


            }
        });


        appConstants.startSpinwheel(MainScreenActivity.this,false,true);
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    getProfileRequest(urlClass.getProfileURL+"profile", null);

                } catch (Exception xx) {
                    xx.toString();
                    appConstants.stopSpinWheel();
                }
            }
        }).start();

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                    getCartCountReq();
//
//
//            }
//        }).start();


    }


//
//    private final LocationListener locationListener = new LocationListener()
//    {
//        public void onLocationChanged(Location location)
//        {
//            appConstants.longt = location.getLongitude();
//            appConstants.latit = location.getLatitude();
//
//        }
//
//        @Override
//        public void onStatusChanged(String provider, int status, Bundle extras) {
//
//        }
//
//        @Override
//        public void onProviderEnabled(String provider) {
//
//        }
//
//        @Override
//        public void onProviderDisabled(String provider) {
//
//        }
//    };

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Intent intent;

        switch (item.getItemId()) {

            case R.id.home_dr:
                fragment = new MainMenuFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.my_profile_dr:

                    fragment = new MyProfileFragment();
                    tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, fragment);
                    tx.addToBackStack(null);
                    tx.commit();
                     break;

//            case R.id.about_dr:
//                fragment = new AboutUsFragment();
//                tx = getSupportFragmentManager().beginTransaction();
//                tx.replace(R.id.main_container, fragment);
//                tx.addToBackStack(null);
//                tx.commit();
//                break;

            case R.id.cart_dr:
                fragment = new MyCartFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;


            case R.id.points_dr:
                fragment = new PointsFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.contactus_dr:
                fragment = new ContactUsFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;


//            case R.id.faq_dr:
//                fragment = new FAQFragment();
//                tx = getSupportFragmentManager().beginTransaction();
//                tx.replace(R.id.main_container, fragment);
//                tx.addToBackStack(null);
//                tx.commit();
//                break;

            case R.id.notification_dr:
                fragment = new NotificationFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.orders_dr:
                fragment = new MyOrderFragment();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.myAddress_dr:
                AddressesFragmnet.comeFromCart="";
                fragment = new AddressesFragmnet();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.world_map_dr:
                fragment = new WorldMapFragmnet();
                tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, fragment);
                tx.addToBackStack(null);
                tx.commit();
                break;

            case R.id.about_dr:
            showAboutDevDialog();
                break;



        }

        drawer.closeDrawer(Gravity.LEFT);
        return true;
    }


    @Override
    public void onBackPressed() {

        try {


            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
            if (drawer.isDrawerOpen(Gravity.LEFT)) {
                drawer.closeDrawer(Gravity.LEFT);
            } else {

                int fragments = getSupportFragmentManager().getBackStackEntryCount();

                if(fragments == 1)
                {
                    toolbar_title.setText(R.string.menu_tab);
                }
                if (fragments == 0) {
                    new AlertDialog.Builder(this)
                            .setIcon(R.mipmap.icon)
                            .setMessage(R.string.exit_msg)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    MainScreenActivity.this.finish();
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();
                }

                else {

                    super.onBackPressed();
                }

            }
        }
        catch (Exception xx){}

    }

    private void setInitialLanguage(String language) {
        Locale locale = new Locale(language);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
        onConfigurationChanged(conf);


        sharedPref.setStringPreference(getApplicationContext(),appConstants.locale_KEY,language);
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        startActivity(i);
    }

    private void logoutRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            sharedPref.setStringPreference(getApplicationContext(), appConstants.userID_KEY, "");
                            sharedPref.setStringPreference(getApplicationContext(), appConstants.userName_KEY, "");
                            sharedPref.setStringPreference(getApplicationContext(), appConstants.userPassword_KEY, "");
                            sharedPref.setStringPreference(getApplicationContext(), appConstants.userCountry_KEY, "");
                            sharedPref.setStringPreference(getApplicationContext(), appConstants.notificationStatus_KEY, "");
                            sharedPref.setBooleanPreference(getApplicationContext(), appConstants.isLoggedIn, false);

                            Intent i = getPackageManager().getLaunchIntentForPackage(getPackageName());
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                            startActivity(i);

                        }
                        else
                        {
                            final String status_message=jsonObjResp.getString("status_message");
                            Toast.makeText(getApplicationContext(), status_message, Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    private void getProfileRequest(String urlPost, final JSONObject jsonObjectRequest) {


        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONObject obj=jsonObjResp.getJSONObject("data");
                            userName=obj.getString("name");
                            profileNameTXT.setText(userName);

//                            appConstants.profilePicture=obj.getString("profilePicture");
//
//                            Glide.with(MainScreenActivity.this).load(appConstants.profilePicture)
//                                    .apply(RequestOptions.placeholderOf(R.drawable.prof_img))
//                                    .apply(RequestOptions.circleCropTransform()).into(profile_image);


                        }




                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                    }


                }
                catch (Exception e)
                {
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                appConstants.stopSpinWheel();

            }


//
        }

        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    public static void getCartCountReq() {

        JsonObjectRequest jsonObjReq;
        String urlPost= urlClass.getCartURL;
       JSONObject jsonObjectRequest=new JSONObject();


        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        cartArrayList=new ArrayList<>();


                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONObject mainObject=new JSONObject();
                            try{mainObject=jsonObjResp.getJSONObject("data");}catch (Exception xx){}

                            JSONArray itemsArr=mainObject.getJSONArray("items");

                            String cartIDd=mainObject.getString("cartId");

                            String total=mainObject.getString("total");


                            for (int i=0;i<itemsArr.length();i++)
                            {
                                JSONObject obj=itemsArr.getJSONObject(i);

                                String itemID=obj.getString("itemId");
                                String cartItemId=obj.getString("cartItemId");
                                String name=obj.getString("name");
                                String img=obj.getString("itemImage");
                                String price=obj.getString("price");
                                String qty=obj.getString("quantity");
                                JSONArray ingrediant=obj.getJSONArray("ingredients");
                                JSONArray extra=new JSONArray();


                                cartModel=new MyCartModel(itemID,cartItemId,img,name,price,qty,ingrediant,extra,"");
                                cartArrayList.add(cartModel);

                            }


                            if(cartArrayList.size()>0)
                            {
                                cartCountTXT.setText(String.valueOf(cartArrayList.size()));
                            }
                            else
                            {
                                cartCountTXT.setText(String.valueOf(cartArrayList.size()));
                            }

                        }


                    }



                }
                catch (Exception e)
                {
                    cartCountTXT.setText(String.valueOf(cartArrayList.size()));
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {


            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(myActivity);
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    public static void showAboutDevDialog()
    {

        dialog = new Dialog(myActivity, android.R.style.Theme_Holo_Light_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.about_develper_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        dialog.show();

    }

}