package com.firefly.android.app.fragment;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.models.CityWithRegionsModel;
import com.firefly.android.app.models.CountryModel;
import com.firefly.android.app.models.RegionsModel;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by HP on 5/3/2017.
 */

public class AddAddressFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    View v;
    EditText adressDescTXT, buildingNOTXT, flatNOTXT, apartmentTXT,nameTXT;

    Button submitBTN,sendCurrentLocBTN;
    Spinner countrySpin, citySspin,regionSpin;
    Switch autoSwitch;

    LocationManager locationManager;
    Location location;
    public static double currntlatti = 0.0;
    public static double currntlongi = 0.0;

    SupportMapFragment mapFrag;
    SharedPrefsUtils sharedPref;
    private GoogleMap mGoogleMap;
    Boolean mMapIsTouched;
    LinearLayout addressValuesLO;

    ArrayList<CountryModel> countryArrayList;
    CountryModel countryModel;
    String [] countryArr;
    Spinner countrySpinner;

    public static String [] cityArr;
    public static ArrayList<CityWithRegionsModel> cityArrayList;
    CityWithRegionsModel cityModel;

    JSONArray regionArr;
    public static String [] regionArrStr;
    RegionsModel regionsModel;
    ArrayList<RegionsModel> regionsModelArrayList;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.add_address_layout, container, false);

        MainScreenActivity.toolbar_title.setText(R.string.deleviry);

        locationManager = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);

        try {


            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 11);

            } else {

                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    currntlatti = location.getLatitude();
                    currntlongi = location.getLongitude();

                } else {
                }
            }
        }
        catch (Exception xx){}


        mMapIsTouched = false;
        mapFrag = (SupportMapFragment)  getChildFragmentManager().findFragmentById(R.id.addressMap);
        mapFrag.getMapAsync(this);

        try {



            sendCurrentLocBTN= v.findViewById(R.id.sendCurrentLocBTN);
            adressDescTXT = v.findViewById(R.id.adressDescTXT);
            nameTXT= v.findViewById(R.id.nameTXT);
            //postalCodeTXT = v.findViewById(R.id.postalCodeTXT);
            buildingNOTXT =  v.findViewById(R.id.buildingNOTXT);
            flatNOTXT =  v.findViewById(R.id.flatNOTXT);
            apartmentTXT=  v.findViewById(R.id.apartmentTXT);
            submitBTN = v.findViewById(R.id.editAddresBTN_edit);
            countrySpinner=v.findViewById(R.id.countrySpinner);
//            countrySpin= v.findViewById(R.id.countrySpin);
            citySspin= v.findViewById(R.id.citySspin);
            regionSpin= v.findViewById(R.id.regionSpin);
            autoSwitch= v.findViewById(R.id.autoSwitch);
            addressValuesLO= v.findViewById(R.id.addressValuesLO);


            autoSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                    {
                        sendCurrentLocBTN.performClick();
                        adressDescTXT.setEnabled(false);
                        buildingNOTXT.setEnabled(false);
                        flatNOTXT.setEnabled(false);
                        apartmentTXT.setEnabled(false);
                    }
                    else
                    {
                        adressDescTXT.setEnabled(true);
                        buildingNOTXT.setEnabled(true);
                        flatNOTXT.setEnabled(true);
                        apartmentTXT.setEnabled(true);
                    }
                }
            });


            citySspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long idlong) {

                    if(regionArr!=null) {
                        try
                        {
                            regionsModelArrayList=new ArrayList<>();
                            regionArrStr=new String[regionArr.length()];

                            for (int i = 0; i < regionArr.length(); i++)
                            {

                                JSONObject obj = regionArr.getJSONObject(i);

                                String id=obj.getString("id");
                                String name=obj.getString("name");
                                String hash=obj.getString("regionHash");
                                String delivery=obj.getString("deliveryPrice");

                                regionsModel=new RegionsModel(id,name,delivery,hash);
                                regionsModelArrayList.add(regionsModel);
                                regionArrStr[i]=name;

                            }

                            ArrayAdapter<String> regionAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_black,regionArrStr);
                            regionSpin.setAdapter(regionAdapter);
                        }
                        catch (Exception xx){}

                        }
                        else
                        {

                        }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            sendCurrentLocBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    appConstants.startSpinwheel(getActivity(),false,true);
                    new Thread(new Runnable()
                    {
                        @Override
                        public void run() {

                            try {
                                if(currntlatti==0.0 ||currntlatti==0)
                                {
                                    appConstants.stopSpinWheel();
                                    Toast.makeText(getContext(), getResources().getString(R.string.enable_gps), Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
                                List<Address> addresses = geo.getFromLocation(currntlatti, currntlongi, 1);

                                if (addresses.size() > 0)
                                {


                                    appConstants.stopSpinWheel();
//                            currntlongi = addresses.get(0).getLongitude();
//                            currntlatti = addresses.get(0).getLatitude();
                                    adressDescTXT.setText(addresses.get(0).getAddressLine(0));
//                postalCodeTXT.setText(addresses.get(0).getCountryName());
//                adressNameTXT.setText(addresses.get(0).getCountryName());
//                adressNameTXT.setText(addresses.get(0).getCountryName());
//                area = addresses.get(0).getCountryName();
//                name = addresses.get(0).getAddressLine(0);
//                desc=addresses.get(0).getFeatureName();
//                street=addresses.get(0).getFeatureName();
//                direction=addresses.get(0).getAddressLine(0);
//                building=addresses.get(0).getFeatureName();


                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }).start();



                }
            });

            submitBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkRule()) {

                        appConstants.startSpinwheel(getActivity(),false,true);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try
                                {

                                    JSONObject obj=new JSONObject();

                                    obj.putOpt("countryID",Integer.parseInt(appConstants.countryID));
                                    obj.putOpt("cityID",Integer.parseInt(cityArrayList.get(citySspin.getSelectedItemPosition()).getId()));
                                    obj.putOpt("regionID",Integer.parseInt(regionsModelArrayList.get(regionSpin.getSelectedItemPosition()).getId()));
                                    obj.putOpt("name",nameTXT.getText().toString().trim());
                                    obj.putOpt("address",adressDescTXT.getText().toString().trim());
                                    obj.putOpt("buildingNumber",buildingNOTXT.getText().toString().trim());
                                    obj.putOpt("flatNO",flatNOTXT.getText().toString().trim());
                                    obj.putOpt("apartment",apartmentTXT.getText().toString().trim());
                                    //obj.putOpt("postalCode",postalCodeTXT.getText().toString().trim());
                                    obj.putOpt("latitude",currntlatti);
                                    obj.putOpt("longitude",currntlongi);


                                    addAddressRequest(urlClass.addUserAddressURL, obj);


                                } catch (Exception xx) {
                                    xx.toString();
                                }
                            }
                        }).start();


                    }

                }
            });
        }
        catch (Exception xx)
        {xx.toString();}



//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                try {
//
//                    String url = urlClass.getCountryURL;
//                    getCountriestReq(url, null);
//
//                } catch (Exception xx)
//                {
//                    String xxx=xx.toString();
//                }
//            }
//        }).start();


        appConstants.startSpinwheel(getActivity(),false,true);
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getCityURL;
                    getCitiesReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();


        return v;
    }


    private  boolean checkRule() {
        if (nameTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Please check the address", Toast.LENGTH_SHORT).show();
            return false;
        }
//        if (buildingNOTXT.getText().toString().trim().equals("")) {
//            Toast.makeText(getActivity(), "Please check the building number", Toast.LENGTH_SHORT).show();
//            return false;
//        }

        return  true;
    }

    private void addAddressRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            //Toast.makeText(getActivity(), "Added", Toast.LENGTH_LONG).show();
                            getActivity().getSupportFragmentManager().popBackStack();

                        }
                        else
                        {
                            final String status_message=jsonObjResp.getString("status_message");
                            Toast.makeText(getActivity(), status_message, Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void getCountriestReq(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        countryArrayList=new ArrayList<>();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONArray mainArr=jsonObjResp.getJSONArray("data");
                            countryArr=new String[mainArr.length()];

                            for (int i=0;i<mainArr.length();i++)
                            {

                                JSONObject obj=mainArr.getJSONObject(i);

                                String id=obj.getString("id");
                                String name=obj.getString("name");
                                String currency=obj.getString("currency");
                                String tax=obj.getString("tax");
                                String code=obj.getString("countryCode");
                                String phoneCode=obj.getString("phoneCode");

                                countryModel=new CountryModel(id,name,currency,tax,code,phoneCode);
                                countryArrayList.add(countryModel);
                                countryArr[i]=name;

                            }

                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, countryArr);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            countrySpinner.setAdapter(spinnerArrayAdapter);


                        }


                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    private void getCitiesReq(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        cityArrayList=new ArrayList<>();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONArray mainArr=jsonObjResp.getJSONArray("data");
                            cityArr=new String[mainArr.length()];

                            for (int i=0;i<mainArr.length();i++)
                            {

                                JSONObject obj=mainArr.getJSONObject(i);

                                String id=obj.getString("id");
                                String name=obj.getString("name");
                                String country=obj.getString("country");
                                String short_code=obj.getString("short_code");
                                regionArr=new JSONArray();
                                try
                                {
                                    regionArr=obj.getJSONArray("regions");
                                }
                                catch (Exception xx){}

                                cityModel=new CityWithRegionsModel(id,country,name,short_code,regionArr);
                                cityArrayList.add(cityModel);
                                cityArr[i]=name;

                            }

                          ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_black,cityArr);
                          citySspin.setAdapter(cityAdapter);

                        }


                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }


//    @Override
//    public void onMapLongClick(LatLng point) {
//        try {
//            Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
//            List<Address> addresses = geo.getFromLocation(point.latitude, point.longitude, 1);
//
//            if (addresses.size() > 0) {
//
//
//
//                currntlongi = addresses.get(0).getLongitude();
//                currntlatti = addresses.get(0).getLatitude();
//
//                adressNameTXT.setText(addresses.get(0).getAddressLine(0));
//
//
//                MarkerOptions marker = new MarkerOptions().position(new LatLng(point.latitude, point.longitude)).title(addresses.get(0).getCountryName());
//                mGoogleMap.clear();
//
//                mGoogleMap.addMarker(marker);
//
//                mMapIsTouched = true;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public void onMapReady(GoogleMap googleMap) {


        mGoogleMap = googleMap;
        if (mGoogleMap != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder().target(
                    new LatLng(currntlatti, currntlongi)).zoom(13).build();
            MarkerOptions marker = new MarkerOptions().position(new LatLng(currntlatti, currntlongi)).title("You Location");
            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_new));

            googleMap.addMarker(marker);
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

       // mGoogleMap.setOnMapLongClickListener(this);
    }

}


