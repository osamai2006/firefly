package com.firefly.android.app.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.adapters.BurgerMeatAdapter;
import com.firefly.android.app.adapters.ExtrasAdapter;
import com.firefly.android.app.adapters.IngrediantsAdapter;
import com.firefly.android.app.adapters.PotatoAdapter;
import com.firefly.android.app.models.BurgerModel;
import com.firefly.android.app.models.DrinksModel;
import com.firefly.android.app.models.ExtrasModel;
import com.firefly.android.app.models.IngrediantsModel;
import com.firefly.android.app.models.PotatoModel;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 5/9/2017.
 */

public class EditCartFragmentNew extends Fragment implements View.OnClickListener {

    View view;
    TextView mealWeghitTXT;
    static TextView mealPriceTXT;
    TextView qtyTXT;
    TextView openCutomizedBTN;
    Button addToCartBTN;
    ImageView mealImage_details,minusBTN,plusBTN,mealNameImg;
    LinearLayout slideUpBTN1_LO,slideUpBTN2_LO;
    LinearLayout customizaMainLO;
    SharedPrefsUtils sharedPref;
    EditText notesTXT;

    Dialog dialog;

    GridView ingredianGRIDView,extraGV,potatoGRIDView,burgerGRIDView,drinksGRIDView;

    static IngrediantsAdapter ingrediantsAdapter;
    static ArrayList<IngrediantsModel> ingrediantsArrayList;
    IngrediantsModel ingrediantsModel;

    static ExtrasAdapter extraAdapter;
    static ArrayList<ExtrasModel> extraArrayList;
    ExtrasModel extrasModel;

    static PotatoAdapter potatoAdapter;
    static ArrayList<PotatoModel> potatoArrayList;
    PotatoModel potatoModel;
    ArrayList<String> potataNameArr=new ArrayList<>();
    ArrayList<String> potataIDArr=new ArrayList<>();


    static BurgerMeatAdapter burgerMeatAdapter;
    static ArrayList<BurgerModel> burgerModelsArrayList;
    BurgerModel burgerModel;
    ArrayList<String> burgerNameArr=new ArrayList<>();
    ArrayList<String> burgerIDArr=new ArrayList<>();


    static ArrayList<DrinksModel> drinksArrayList;
    DrinksModel drinksModel;
    ArrayList<String> drinksNameArr=new ArrayList<>();
    ArrayList<String> drinksIDArr=new ArrayList<>();


    static int qty;
    public static String name="",price="",mealPrice="",meat="",image="",mealID="",totalPrice="";
    Dialog deleviryDialog;

    String selectedPototaID="";
    String selectedDrinkID="";
    String selectedBurgMeatID="";
    static String mealSandwichFlag="1";
    String deleveryOption="";
    RadioGroup mealSandRG;



    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.edit_cart_fragment_layout, container, false);

        MainScreenActivity.toolbar_title.setText(R.string.menu_tab);

        mealNameImg=view.findViewById(R.id.mealNameImg);
        mealWeghitTXT=view.findViewById(R.id.mealWeghitTXT);
        mealPriceTXT=view.findViewById(R.id.mealPriceTXT);
        mealPriceTXT.setText(totalPrice + " JD");
        qtyTXT=view.findViewById(R.id.qtyTXT);
        qtyTXT.setText(String.valueOf(qty));



        mealImage_details=view.findViewById(R.id.mealImage_details);
        minusBTN=view.findViewById(R.id.minusBTN);
        plusBTN=view.findViewById(R.id.plusBTN);
        slideUpBTN1_LO=view.findViewById(R.id.slideUpBTN1_LO);
        slideUpBTN2_LO=view.findViewById(R.id.slideUpBTN2_LO);
        addToCartBTN=view.findViewById(R.id.addToCartBTN);
        mealSandRG=view.findViewById(R.id.mealSandRG);
        notesTXT=view.findViewById(R.id.notesTXT);
        customizaMainLO=view.findViewById(R.id.customizaMainLO);
        openCutomizedBTN=view.findViewById(R.id.openCutomizedBTN);
        openCutomizedBTN.setOnClickListener(this);
        customizaMainLO.setVisibility(View.GONE);


        ingredianGRIDView=view.findViewById(R.id.ingredianGRIDView);
        ingredianGRIDView.setVisibility(View.VISIBLE);

        extraGV=view.findViewById(R.id.extraGV);
        extraGV.setVisibility(View.VISIBLE);

        potatoGRIDView=view.findViewById(R.id.potatoGRIDView);
        //potatoGRIDView.setVisibility(View.VISIBLE);

        drinksGRIDView=view.findViewById(R.id.drinksGRIDView);
        //drinksGRIDView.setVisibility(View.VISIBLE);

        burgerGRIDView=view.findViewById(R.id.burgerGRIDView);
        //burgerGRIDView.setVisibility(View.VISIBLE);



        mealWeghitTXT.setText(meat);
       // Glide.with(getActivity()).load( image).apply(RequestOptions.circleCropTransform()).into(mealImage_details);
        //Picasso.with(getActivity()).load(image).placeholder(R.drawable.bg_burger).into(mealImage_details);

        plusBTN.setOnClickListener(this);
        minusBTN.setOnClickListener(this);
        addToCartBTN.setOnClickListener(this);
        slideUpBTN1_LO.setOnClickListener(this);
        slideUpBTN2_LO.setOnClickListener(this);

        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getenuDetailsURL+"/"+mealID+"/details";
                    getMenuDetailsReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();



        if(mealSandwichFlag.equalsIgnoreCase("2"))
        {
             mealSandRG.check(R.id.mealRB);
        }
        else
        {
            mealSandRG.check(R.id.sandwichRB);
        }


        mealSandRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId==R.id.mealRB)
                {
                    mealSandwichFlag="2";
                    mealPriceTXT.setText(mealPrice + " JD");
                    mealPriceTXT.setText(String.valueOf(calculateTotal(mealSandwichFlag))+" JD");
                }
                else   if(checkedId==R.id.sandwichRB)
                {
                    mealSandwichFlag="1";
                    mealPriceTXT.setText(price + " JD");
                    mealPriceTXT.setText(String.valueOf(calculateTotal(mealSandwichFlag))+" JD");
                }

            }
        });


        return view;
    }


    @Override
    public void onClick(View v)
    {
        if(v.getId()==R.id.openCutomizedBTN)
        {
            if (customizaMainLO.getVisibility() == View.VISIBLE) {
                customizaMainLO.setVisibility(View.GONE);

            } else {
                customizaMainLO.setVisibility(View.VISIBLE);
            }
        }

//        if(v.getId()==R.id.slideUpBTN1_LO)
//        {
//            if (ingredianGRIDView.getVisibility() == View.VISIBLE) {
//                ingredianGRIDView.setVisibility(View.GONE);
//
//            } else {
//                ingredianGRIDView.setVisibility(View.VISIBLE);
//            }
//        }
//        if(v.getId()==R.id.slideUpBTN2_LO)
//        {
//            if (extraGV.getVisibility() == View.VISIBLE) {
//                extraGV.setVisibility(View.GONE);
//
//            } else {
//                extraGV.setVisibility(View.VISIBLE);
//            }
//        }
        if(v.getId()==R.id.plusBTN)
        {
            ++qty;
            qtyTXT.setText(String.valueOf(qty));
            mealPriceTXT.setText(String.valueOf(calculateTotal(mealSandwichFlag))+" JD");
        }

        if(v.getId()==R.id.minusBTN)
        {
            if (qty == 1)
            {
                qtyTXT.setText(String.valueOf(qty));
            }
            else
                {
                --qty;
                qtyTXT.setText(String.valueOf(qty));
            }
            mealPriceTXT.setText(String.valueOf(calculateTotal(mealSandwichFlag))+" JD");
        }
        if(v.getId()==R.id.addToCartBTN)
        {
            boolean isLoged = sharedPref.getBooleanPreference(getActivity(), appConstants.isLoggedIn, false);
            String userID = sharedPref.getStringPreference(getActivity(), appConstants.userID_KEY);

             appConstants.startSpinwheel(getActivity(),false,true);

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            String url = urlClass.updateCartURL;

//                            JSONObject prepOption=new JSONObject();
//                            prepOption.putOpt("id",selectedBurgMeatID);

//                            JSONObject potatoOption=new JSONObject();
//                            potatoOption.putOpt("id",selectedPototaID);

//                            JSONObject drinkOption=new JSONObject();
//                            drinkOption.putOpt("id",selectedDrinkID);

                            JSONArray addOnsOptionArr=new JSONArray();
                            JSONObject addOnsOptionObj=new JSONObject();

                            String addOnsString="";
                            for(int i=0;i<extraArrayList.size();i++)
                            {
                                if(extraArrayList.get(i).getChecked().equalsIgnoreCase("true"))
                                {
//                                    addOnsOptionObj.putOpt("id",extraArrayList.get(i).getId());
//                                    addOnsOptionArr.put(i,addOnsOptionObj);
                                    addOnsString=addOnsString+extraArrayList.get(i).getId()+",";

                                }
                            }

                            if(addOnsString.endsWith(","))
                            {
                                addOnsString= addOnsString.substring(0, addOnsString.length() - 1);
                            }



                            JSONObject mainObj = new JSONObject();
                            mainObj.putOpt("itemId", mealID);
                           // mainObj.putOpt("deliveryMethod", deleveryOption);
                            mainObj.putOpt("itemType", mealSandwichFlag);
                            mainObj.putOpt("note", notesTXT.getText().toString().trim());
                            mainObj.putOpt("quantity", qtyTXT.getText().toString().trim());
                            // mainObj.putOpt("userID", userID);
                            mainObj.putOpt("preparationOptions",selectedBurgMeatID);
                            mainObj.put("potato", selectedPototaID);
                            mainObj.put("drink", selectedDrinkID);
                            mainObj.put("addons", addOnsString);

                            updateCartRequest(url, mainObj);

                        }
                        catch (Exception xx){}
                    }
                }).start();




        }


    }

    private void fillIngrediants(JSONArray ingredArr ) {

        try {
            appConstants.stopSpinWheel();
            ingrediantsArrayList = new ArrayList<>();
            ingrediantsArrayList.clear();

            for (int i = 0; i < ingredArr.length(); i++) {
                JSONObject obj = ingredArr.getJSONObject(i);
                String id = obj.getString("id");
                String img = obj.getString("image");
                String name = obj.getString("name");
                String checked = "true";

                ingrediantsModel = new IngrediantsModel(id, img, name, checked);
                ingrediantsArrayList.add(ingrediantsModel);


            }

            ingrediantsAdapter = new IngrediantsAdapter(getActivity(), ingrediantsArrayList);
            ingredianGRIDView.setAdapter(ingrediantsAdapter);
        }
        catch (Exception xx){}

    }
    private void fillExtra(JSONArray extrasArr ) {

        try
        {
        appConstants.stopSpinWheel();
        extraArrayList = new ArrayList<>();
        extraArrayList.clear();

        for (int i = 0; i < extrasArr.length(); i++) {
            JSONObject obj = extrasArr.getJSONObject(i);
            String id = obj.getString("id");
            String img = obj.getString("image");
            String name = obj.getString("name");
            String price = obj.getString("price");
            String checked = "false";


            extrasModel = new ExtrasModel(id, img, name, price, checked);
            extraArrayList.add(extrasModel);


        }

            extraAdapter = new ExtrasAdapter(getActivity(), extraArrayList,"edit");
        extraGV.setAdapter(extraAdapter);

    }
    catch(Exception xx){}

    }





    private void getMenuDetailsReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();

                                    JSONObject jsonObjResp = new JSONObject(response.toString());
                                    final String status=jsonObjResp.getString("success");

                                    if(status.equalsIgnoreCase("true"))
                                    {
                                            JSONObject obj=jsonObjResp.getJSONObject("data");

                                            String id = obj.getString("id");
                                            String name = obj.getString("name");
                                            String nameImg = obj.getString("imageName");
                                            String img =obj.getString("image");
                                            price = obj.getString("price");
                                            mealPrice= obj.getString("mealPrice");
                                            String meat = obj.getString("meat");
                                            String country = obj.getString("country");
                                            JSONArray ingredArr = obj.getJSONArray("ingredients");
                                            JSONArray extrasArr =obj.getJSONArray("extras");
                                            JSONArray potatoArr = obj.getJSONArray("Potato");
                                            JSONArray drinksArr =obj.getJSONArray("drinks");
                                            JSONArray burgerArr =obj.getJSONArray("burger");
                                            Picasso.with(getActivity()).load(nameImg).into(mealNameImg);


                                            ingrediantsArrayList=new ArrayList<>();
                                            extraArrayList=new ArrayList<>();
                                            potatoArrayList=new ArrayList<>();
                                            drinksArrayList=new ArrayList<>();
                                            burgerModelsArrayList=new ArrayList<>();

                                            fillIngrediants(ingredArr);
                                            fillExtra(extrasArr);
                                            //fillPotato(potatoArr);
                                            //fillDrinks(drinksArr);
                                            //fillBurgerMeat(burgerArr);







                                    }

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String bearer = "Bearer ".concat(appConstants.userToken);
                    Map<String, String> headersSys = super.getHeaders();
                    Map<String, String> headers = new HashMap<String, String>();
                    headersSys.remove("Authorization");
                    headers.put("Authorization", bearer);
                    headers.put("language", appConstants.current_language);
                    headers.putAll(headersSys);
                    return headers;
                }
            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }
    public static void updateExtraChecked(int position)
    {
        extraAdapter.notifyDataSetChanged();
        mealPriceTXT.setText(String.valueOf(calculateTotal(mealSandwichFlag))+" JD");
    }
    public static void updateIngrediantChecked(int position)
    {
        ingrediantsAdapter.notifyDataSetChanged();
    }
    public static void updatePotatoChecked(int position)
    {
        potatoAdapter.notifyDataSetChanged();
    }
    public static void updateBurgerChecked(int position)
    {
        burgerMeatAdapter.notifyDataSetChanged();
    }
    private void updateCartRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            //Toast.makeText(getActivity(), "Added Success", Toast.LENGTH_LONG).show();
                            //showDialogMethodPop();
                            // MainScreenActivity.getCartCountReq();
                            getActivity().getSupportFragmentManager().popBackStack();


                        }
                        else
                        {
                            final String status_message=jsonObjResp.getString("status_message");
                            Toast.makeText(getActivity(), status_message, Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                //headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }


    private void showDialogMethodPop() {

        dialog = new Dialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.gocart_menu_pop_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        RadioGroup rg= dialog.findViewById(R.id.rg);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                if(checkedId==R.id.goCartRB)
                {
                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new MyCartFragment());
                    tx.addToBackStack(null);
                    tx.commit();

                    dialog.dismiss();
                }

                if(checkedId==R.id.goMenuRB)
                {

                    myContext.getSupportFragmentManager().popBackStack();

                    dialog.dismiss();

                }


            }
        });


        dialog.show();

    }

    public static double  calculateTotal(String sandwichFlag)
    {
        double total=0;
        double totalExtras=0;

        try
        {
            for(int i=0;i<extraArrayList.size();i++)
            {
                if(extraArrayList.get(i).getChecked().equalsIgnoreCase("true"))
                {
                    double extrsPrice=Double.parseDouble(extraArrayList.get(i).getPrice());
                    totalExtras+=extrsPrice*qty;
                }

            }

            if(sandwichFlag.equals("1"))
            {
                total = Double.parseDouble(price) * qty;
            }
            else
            {
                total = Double.parseDouble(mealPrice) * qty;

            }
        }
        catch (Exception xx){}

        return total+totalExtras;
    }



}
