package com.firefly.android.app.others;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;

import org.json.JSONObject;



public class VolleyRequestClass {



    public ResponseHandler responseHandler;




    public void objectRequest(final JSONObject jsonObjectRequest, final String urlRequest,final String apiName, final Context context )
    {
        appConstants.startSpinwheel(context,false,true);
        new Thread(new Runnable() {
            @Override
            public void run()
            {
                try
                {
                    JsonObjectRequest jsonObjReq;
                    jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlRequest, jsonObjectRequest, new Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response) {
                            try
                            {
                                appConstants.stopSpinWheel();

                                if(response !=null)
                                {
                                    responseHandler.successRequest(response,apiName);
                                }


                            }
                            catch (Exception error)
                            {
                                Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                                appConstants.stopSpinWheel();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                            responseHandler.volleyError(error.getMessage(),apiName);
                            appConstants.stopSpinWheel();
                        }
                    });
                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                            8000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(jsonObjReq);

                }
                catch (Exception ex)
                {
                    appConstants.stopSpinWheel();
                }

            }
        }).start();


    }




    public interface ResponseHandler{

       void successRequest(JSONObject response,String apiName);
       void volleyError(String msg,String apiName);
    }

}
