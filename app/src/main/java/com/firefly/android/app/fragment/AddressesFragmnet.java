package com.firefly.android.app.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.adapters.AddressesAdapter;
import com.firefly.android.app.models.AddressesModel;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 5/3/2018.
 */

public class AddressesFragmnet extends Fragment {

    RecyclerView addressesLV;
    Dialog deleviryDialog;

    static ArrayList<AddressesModel> arrayList;
    AddressesModel model;
    static AddressesAdapter adapter;
    public static String comeFromCart="";


    Button addAddressBTN;



    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v=inflater.inflate(R.layout.addresses_activity_layout, container, false);


        MainScreenActivity.toolbar_title.setText(R.string.my_address_menu);

        addressesLV= v.findViewById(R.id.addressesLV);
        addAddressBTN=v.findViewById(R.id.addAddressBTN);


        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getUserAddressURL;
                    getAddressesReq(url, null);

                } catch (Exception xx){}

            }
        }).start();



        addAddressBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, new AddAddressFragment());
                tx.addToBackStack(null);
                tx.commit();
            }
        });

        return v;


    }


    private void getAddressesReq(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        arrayList=new ArrayList<>();


                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONObject mainObject=jsonObjResp.getJSONObject("data");
                            JSONArray itemsArr=mainObject.getJSONArray("deliveryPoints");


                            for (int i=0;i<itemsArr.length();i++)
                            {
                                JSONObject obj=itemsArr.getJSONObject(i);

                                String id=obj.getString("id");
                                String name=obj.getString("name");



                                model=new AddressesModel(id,name,"","","","","","","","","");
                                arrayList.add(model);

                            }

                            adapter = new AddressesAdapter(getActivity(),arrayList);
                            addressesLV.setAdapter(adapter);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            addressesLV.setLayoutManager(layoutManager);

                        }


                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        //Toast.makeText(getActivity(), "server error", Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    //Toast.makeText(getActivity(),"server error", Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }


    public static void removeAddressReq(final String itemID, final int pos) {

        appConstants.startSpinwheel(myContext, false, true);

        final String url=urlClass.deleteAddressURL+itemID+"/delete-address";

        new Thread(new Runnable() {
            @Override
            public void run() {
                JsonObjectRequest jsonObjReq;
                JSONObject obj=new JSONObject();
                try
                {
                    obj.putOpt("itemId",itemID);
                }
                catch (Exception xx){}

                jsonObjReq = new JsonObjectRequest(Request.Method.DELETE, url, obj, new Response.Listener<JSONObject>()
                {

                    @Override
                    public void onResponse(JSONObject response) {

                        try
                        {
                            if(response !=null)
                            {
                                appConstants.stopSpinWheel();

                                JSONObject jsonObjResp = new JSONObject(response.toString());
                                final String status=jsonObjResp.getString("success");

                                if(status.equalsIgnoreCase("true"))
                                {
                                    arrayList.remove(pos);
                                    adapter.notifyDataSetChanged();

                                }


                            }
                            else
                            {
                                appConstants.stopSpinWheel();
                                Toast.makeText(myContext,myContext. getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            }


                        }
                        catch (Exception e)
                        {
                            Toast.makeText(myContext,myContext. getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                            appConstants.stopSpinWheel();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Toast.makeText(myContext,myContext. getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                        appConstants.stopSpinWheel();

                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        String bearer = "Bearer ".concat(appConstants.userToken);
                        Map<String, String> headersSys = super.getHeaders();
                        Map<String, String> headers = new HashMap<String, String>();
                        headersSys.remove("Authorization");
                        headers.put("Authorization", bearer);
                        headers.put("language", appConstants.current_language);
                        headers.putAll(headersSys);
                        return headers;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(myContext);
                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                        appConstants.appTimeOut,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(jsonObjReq);
            }
        }).start();

    }


    public static void goToConfirmScreen()
    {

        ConfirmationFragmentNew.checkoutMethod="1";
//        ConfirmationFragmentNew.deliveryAddressName=adressNameTXT.getText().toString().trim();
//        ConfirmationFragmentNew.deliveryBuildingNum=buildingNOTXT.getText().toString().trim();

      //  ConfirmationFragmentNew.deleveryPrice="";

        FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main_container, new ConfirmationFragmentNew());
        tx.addToBackStack(null);
        tx.commit();
    }





}
