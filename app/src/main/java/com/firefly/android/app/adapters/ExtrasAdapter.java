package com.firefly.android.app.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firefly.android.app.R;

import com.firefly.android.app.fragment.EditCartFragmentNew;
import com.firefly.android.app.fragment.MenuDetailsFragment;
import com.firefly.android.app.models.ExtrasModel;
import com.firefly.android.app.models.IngrediantsModel;

import java.util.ArrayList;

/**
 * Created by HP on 5/4/2017.
 */

public class ExtrasAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ExtrasModel> arrayList;
    String addEdit="";


    public ExtrasAdapter(Context context, ArrayList<ExtrasModel> categorieModelArrayList,String addEdit_) {
        this.context = context;
        this.arrayList = categorieModelArrayList;
        this.addEdit=addEdit_;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View view = convertView;
        try {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null)
            {
                try {
                    view = inflater.inflate(R.layout.extras_grid_item_layout, null);
                }
                catch (Exception xx)
                {
                    xx.getMessage();
                }

            }

            ImageView extraIMG = (ImageView) view.findViewById(R.id.extraIMG);
            TextView extraNameTXT= (TextView) view.findViewById(R.id.extraNameTXT);
            TextView extraPriceTXT= (TextView) view.findViewById(R.id.extraPriceTXT);

            LinearLayout extraMainLO = (LinearLayout) view.findViewById(R.id.extraMainLO);
            Glide.with(context).load( arrayList.get(position).getImage()).apply(RequestOptions.circleCropTransform()).into(extraIMG);

            extraNameTXT.setText(arrayList.get(position).getName());
            extraPriceTXT.setText(arrayList.get(position).getPrice());

            if(arrayList.size()>0) {
                if (arrayList.get(position).getChecked().equalsIgnoreCase("true")) {
                    extraMainLO.setBackgroundResource(R.drawable.checked);
                } else {
                    extraMainLO.setBackgroundResource(R.drawable.unchecked);
                }
            }

            extraMainLO.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if (arrayList.get(position).getChecked().equalsIgnoreCase("true"))
                    {
                        arrayList.get(position).setChecked("false");

                    }
                    else  if (arrayList.get(position).getChecked().equalsIgnoreCase("false"))
                    {
                        arrayList.get(position).setChecked("true");
                    }


                    if(addEdit.equalsIgnoreCase("add"))
                    {
                        MenuDetailsFragment.updateExtraChecked(position);
                    }
                    else   if(addEdit.equalsIgnoreCase("edit"))
                    {
                        EditCartFragmentNew.updateExtraChecked(position);
                    }


                }
            });


            extraIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if (arrayList.get(position).getChecked().equalsIgnoreCase("true"))
                    {
                        arrayList.get(position).setChecked("false");

                    }
                    else  if (arrayList.get(position).getChecked().equalsIgnoreCase("false"))
                    {
                            arrayList.get(position).setChecked("true");
                    }


                    if(addEdit.equalsIgnoreCase("add"))
                    {
                        MenuDetailsFragment.updateExtraChecked(position);
                    }
                    else   if(addEdit.equalsIgnoreCase("edit"))
                    {
                        EditCartFragmentNew.updateExtraChecked(position);
                    }


                }
            });
        }
        catch (Exception xx)
        {}
        return view;
    }

}
