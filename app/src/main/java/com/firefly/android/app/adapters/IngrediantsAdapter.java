package com.firefly.android.app.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firefly.android.app.R;
import com.firefly.android.app.fragment.MenuDetailsFragment;
import com.firefly.android.app.models.IngrediantsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by HP on 5/4/2017.
 */

public class IngrediantsAdapter extends BaseAdapter {

    ProgressDialog progressBar;
    private Context context;
    private ArrayList<IngrediantsModel> arrayList;


    public IngrediantsAdapter(Context context, ArrayList<IngrediantsModel> categorieModelArrayList) {
        this.context = context;
        this.arrayList = categorieModelArrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View view = convertView;
        try {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null)
            {
                view = inflater.inflate(R.layout.ingrediants_grid_item_layout, null);

            }

            ImageView ingredianIMG = (ImageView) view.findViewById(R.id.ingredianIMG);
            TextView ingrediNameTXT= (TextView) view.findViewById(R.id.ingrediNameTXT);
            LinearLayout ingrediantMainLO = (LinearLayout) view.findViewById(R.id.ingrediantMainLO);
            Glide.with(context).load( arrayList.get(position).getImage()).apply(RequestOptions.circleCropTransform()).into(ingredianIMG);

            ingrediNameTXT.setText(arrayList.get(position).getName());

            if (arrayList.get(position).getChecked().equalsIgnoreCase("true"))
            {
                ingrediantMainLO.setBackgroundResource(R.drawable.checked);
            }else{
                ingrediantMainLO.setBackgroundResource(R.drawable.unchecked);
            }


            ingrediantMainLO.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if (arrayList.get(position).getChecked().equalsIgnoreCase("true"))
                    {
                        arrayList.get(position).setChecked("false");

                    }
                    else  if (arrayList.get(position).getChecked().equalsIgnoreCase("false"))
                    {
                        arrayList.get(position).setChecked("true");
                    }

                    MenuDetailsFragment.updateIngrediantChecked(position);
                }
            });


            ingredianIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if (arrayList.get(position).getChecked().equalsIgnoreCase("true"))
                    {
                        arrayList.get(position).setChecked("false");

                    }
                    else  if (arrayList.get(position).getChecked().equalsIgnoreCase("false"))
                    {
                            arrayList.get(position).setChecked("true");
                    }

                     MenuDetailsFragment.updateIngrediantChecked(position);
                }
            });
        }
        catch (Exception xx)
        {}
        return view;
    }

}
