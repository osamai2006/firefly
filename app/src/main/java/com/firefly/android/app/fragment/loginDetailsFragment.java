package com.firefly.android.app.fragment;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.FirbaseServices.MyFirebaseInstanceIDService;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.ForgetPasswordActivity;
import com.firefly.android.app.activity.LoginActivity;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONObject;

/**
 * Created by HP on 5/3/2017.
 */

public class loginDetailsFragment extends Fragment {
    View view;
    TextView forgetPasswordTxt;
    EditText login_mobile_txt,passTXT;
    Button loginBTN;
    Animation animShke;
    SharedPrefsUtils sharedPref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login_details_fragment_layout, container, false);
        login_mobile_txt= (EditText) view.findViewById(R.id.login_mobile_txt);
        passTXT= (EditText) view.findViewById(R.id.password_etxt);
        animShke= AnimationUtils.loadAnimation(getActivity(),R.anim.shake);
        loginBTN= (Button) view.findViewById(R.id.lognBTN);
        loginBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                try
                {

                    if(checkRule())
                    {
                        appConstants.startSpinwheel(getActivity(), false, true);

                        new Thread(new Runnable() {
                            @Override
                            public void run()
                            {

                                try
                                {
                                    String token=sharedPref.getStringPreference(getActivity(),appConstants.mobileToken_KEY);
                                    String url= urlClass.loginURL;

                                    JSONObject Objct = new JSONObject();
                                    Objct.putOpt("phone", login_mobile_txt.getText().toString().trim());
                                    Objct.putOpt("password", passTXT.getText().toString().trim());
                                    Objct.putOpt("deviceToken",token);

                                    loginRequest(url,Objct);


                                }
                                catch (Exception xx)
                                {
                                    xx.toString();
                                    appConstants.stopSpinWheel();
                                }
                            }
                        }).start();
                    }
                }
                catch (Exception xx)
                {
                    xx.toString();
                }
            }
        });

        forgetPasswordTxt= (TextView) view.findViewById(R.id.forget_password_txt);

        forgetPasswordTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(), ForgetPasswordActivity.class);
                startActivity(intent);
            }
        });


        return view;
    }

    private  boolean checkRule() {

        if (login_mobile_txt.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (passTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        return  true;
    }


    private void loginRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

         jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
                 {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONObject obj=jsonObjResp.getJSONObject("data");
                            String userID=obj.getString("userId").toString();
                            String userToken=obj.getString("access_token").toString();

                            appConstants.userToken = userToken;
                            appConstants.userID =userID;

                            sharedPref.setStringPreference(getActivity(), appConstants.userID_KEY,userID);
                            sharedPref.setStringPreference(getActivity(), appConstants.userToken_KEY,userToken);
                            sharedPref.setStringPreference(getActivity(), appConstants.userName_KEY,login_mobile_txt.getText().toString().trim());

                            sharedPref.setStringPreference(getActivity(), appConstants.userPassword_KEY,passTXT.getText().toString().trim());
                            sharedPref.setBooleanPreference(getActivity(), appConstants.isLoggedIn,true);

                            getActivity().finish();
                        }
                        else
                        {
                            appConstants.stopSpinWheel();
                            Toast.makeText(getActivity(), "Login Failed".toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }



}
