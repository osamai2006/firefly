package com.firefly.android.app.models;

import org.json.JSONArray;

public class MainMenuModel {

    private String id;
    private String image;
    private String name;
    private String nameImg;
    private String price;
    private String mealPrice;
    private String meat;
    private String country;
    private JSONArray ingrediants;
    private JSONArray burger;

    public MainMenuModel(String id, String image, String name, String nameImg, String price, String mealPrice, String meat, String country, JSONArray ingrediants, JSONArray burger) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.nameImg = nameImg;
        this.price = price;
        this.mealPrice = mealPrice;
        this.meat = meat;
        this.country = country;
        this.ingrediants = ingrediants;
        this.burger = burger;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameImg() {
        return nameImg;
    }

    public void setNameImg(String nameImg) {
        this.nameImg = nameImg;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMealPrice() {
        return mealPrice;
    }

    public void setMealPrice(String mealPrice) {
        this.mealPrice = mealPrice;
    }

    public String getMeat() {
        return meat;
    }

    public void setMeat(String meat) {
        this.meat = meat;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public JSONArray getIngrediants() {
        return ingrediants;
    }

    public void setIngrediants(JSONArray ingrediants) {
        this.ingrediants = ingrediants;
    }

    public JSONArray getBurger() {
        return burger;
    }

    public void setBurger(JSONArray burger) {
        this.burger = burger;
    }
}
