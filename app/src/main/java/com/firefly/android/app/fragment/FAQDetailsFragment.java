package com.firefly.android.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.adapters.FAQAdapter;
import com.firefly.android.app.models.FAQModel;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FAQDetailsFragment extends Fragment {


    RecyclerView faqLV;

   public static TextView questionTXT,answerTXT;
    public static String question,answer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v=inflater.inflate(R.layout.faq_details_layout, container, false);


        MainScreenActivity.toolbar_title.setText(R.string.faq_dr);

        questionTXT=v.findViewById(R.id.questionTXT);
        answerTXT=v.findViewById(R.id.answerTXT);

        questionTXT.setText(question);
        answerTXT.setText(answer);

        return v;


    }


}
