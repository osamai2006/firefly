package com.firefly.android.app.models;

/**
 * Created by HP on 3/25/2018.
 */

public class CategoryModel {

    private String id;
    private String image;
    private String name;

    public CategoryModel(String id, String name, String image) {
        this.setId(id);
        this.setImage(image);
        this.setName(name);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
