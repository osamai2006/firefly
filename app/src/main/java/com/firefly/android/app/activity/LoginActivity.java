package com.firefly.android.app.activity;

import android.os.Bundle;
import android.os.PowerManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.firefly.android.app.R;
import com.firefly.android.app.adapters.LoginFragmentAdapter;

public class
LoginActivity extends AppCompatActivity {

    TabLayout tabLayout;
   public static ViewPager viewPager;
    LoginFragmentAdapter fragmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(pm.SCREEN_DIM_WAKE_LOCK, "My wakelook");
        wakeLock.acquire();
        wakeLock.release();

        setContentView(R.layout.login_main_layout);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout1);
        viewPager = (ViewPager) findViewById(R.id.pager1);


        tabLayout.addTab(tabLayout.newTab().setText(R.string.login_tab));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.register_tab));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.zahry));
        tabLayout.setTabTextColors(getResources().getColor(R.color.Black),getResources().getColor(R.color.zahry));

        fragmentAdapter = new LoginFragmentAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(fragmentAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab LayoutTab) {

                viewPager.setCurrentItem(LayoutTab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab LayoutTab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab LayoutTab) {

            }
        });
    }
}