package com.firefly.android.app.models;

public class AddressesModel {

    private String id;
    private String name;
    private String countryID;
    private String cityID;
    private String address;
    private String longt;
    private String lat;
    private String flatNum;
    private String houseNum;
    private String apartment;
    private String postalCode;

    public AddressesModel(String id, String name, String countryID, String cityID, String address, String longt, String lat, String flatNum, String houseNum, String apartment, String postalCode) {
        this.setId(id);
        this.setName(name);
        this.setCountryID(countryID);
        this.setCityID(cityID);
        this.setAddress(address);
        this.setLongt(longt);
        this.setLat(lat);
        this.setFlatNum(flatNum);
        this.setHouseNum(houseNum);
        this.setApartment(apartment);
        this.setPostalCode(postalCode);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryID() {
        return countryID;
    }

    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }

    public String getCityID() {
        return cityID;
    }

    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongt() {
        return longt;
    }

    public void setLongt(String longt) {
        this.longt = longt;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getFlatNum() {
        return flatNum;
    }

    public void setFlatNum(String flatNum) {
        this.flatNum = flatNum;
    }

    public String getHouseNum() {
        return houseNum;
    }

    public void setHouseNum(String houseNum) {
        this.houseNum = houseNum;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
}
