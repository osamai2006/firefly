package com.firefly.android.app.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;

import com.firefly.android.app.adapters.CountryAdapter;
import com.firefly.android.app.fragment.SliderFragment;
import com.firefly.android.app.models.CountryModel;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;
import com.mukesh.countrypicker.adapters.CountryListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 5/3/2017.
 */

public class CountryActivity extends Activity
{


    RecyclerView countryLV;

    public static String [] cityArr;
    public static ArrayList<CountryModel> arrayList;
    CountryModel model;
    CountryAdapter adapter;

    static SharedPrefsUtils sharedPref;

   public static Activity activity;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.countries_fragment_layout);
        countryLV=findViewById(R.id.countryLV);

        appConstants.startSpinwheel(CountryActivity.this, false, true);

        activity=this;
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getCountryURL;
                    getCountriesReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();

    }

    private void getCountriesReq(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        arrayList=new ArrayList<>();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONArray mainArr=jsonObjResp.getJSONArray("data");

                            for (int i=0;i<mainArr.length();i++)
                            {

                                JSONObject obj=mainArr.getJSONObject(i);

                                String id=obj.getString("id");
                                String name=obj.getString("name");
                                String zip=obj.getString("phoneCode");


                                model=new CountryModel(id,name,"","","",zip);
                                arrayList.add(model);

                            }

                            adapter = new CountryAdapter(getApplicationContext(),arrayList);
                            countryLV.setAdapter(adapter);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                            countryLV.setLayoutManager(layoutManager);

                        }


                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }


    public static void goToMainScreen(final String countryID) {

       appConstants.countryID=countryID;

        boolean isLogged=sharedPref.getBooleanPreference(activity,appConstants.isLoggedIn,false);

        if(isLogged)
        {
            SliderFragment.editProf=false;
            Intent i = new Intent(activity, MainScreenActivity.class);
            activity.startActivity(i);
            activity.finish();
        }
        else
        {
            Intent i = new Intent(activity, loginDetailsActivity.class);
            activity. startActivity(i);
            activity.finish();
        }

    }
}
