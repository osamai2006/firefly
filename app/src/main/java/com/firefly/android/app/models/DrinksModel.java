package com.firefly.android.app.models;

public class DrinksModel {

    private String id;
    private String name;
    private String price;
    private String checked;

    public DrinksModel(String id, String name, String price, String checked) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.checked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }
}
