package com.firefly.android.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firefly.android.app.R;
import com.firefly.android.app.fragment.SliderFragment;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.VolleyRequestClass;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;
import com.mukesh.countrypicker.fragments.CountryPicker;
import com.mukesh.countrypicker.interfaces.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import static com.firefly.android.app.others.appConstants.stopSpinWheel;

/**
 * Created by HP on 5/3/2017.
 */

public class loginDetailsActivity extends AppCompatActivity implements VolleyRequestClass.ResponseHandler{
    TextView forgetPasswordTxt,countryTXT;
    EditText login_mobile_txt,passTXT;
    Button loginBTN,signUpBTN;
    SharedPrefsUtils sharedPref;

    LoginButton facebookLoginButton;
    ImageView login_fb_imgv,flagImg;
    CallbackManager callbackManager;
    CountryPicker picker;
    String phone;

    VolleyRequestClass requestClass=new VolleyRequestClass();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_details_fragment_layout);

        requestClass.responseHandler=this;

        callbackManager = CallbackManager.Factory.create();
        login_fb_imgv = findViewById(R.id.login_fb_imgv);
        login_fb_imgv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookLoginButton.performClick();
            }
        });
        facebookLoginButton = findViewById(R.id.fb_login_button);
        facebookLoginButton.setBackgroundResource(R.drawable.facebook_btn);
        facebookLoginButton.setText(" ");
        facebookLoginButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        facebookLoginButton.setReadPermissions("public_profile email");
        facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                if (AccessToken.getCurrentAccessToken() != null) {
                    String IDFB = loginResult.getAccessToken().getUserId();
                    RequestData(IDFB);
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {

                String xx=exception.getMessage();
                exception.getMessage();

            }
        });



        login_mobile_txt= (EditText)findViewById(R.id.login_mobile_txt);
        passTXT= (EditText) findViewById(R.id.password_etxt);
        loginBTN= (Button) findViewById(R.id.lognBTN);
        signUpBTN= (Button) findViewById(R.id.signUpBTN);
        forgetPasswordTxt= (TextView) findViewById(R.id.forget_password_txt);
        countryTXT=(TextView) findViewById(R.id.countryTXT);
        flagImg= findViewById(R.id.flagImg);

        loginBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                try
                {

                    if(checkRule())
                    {
                        appConstants.startSpinwheel(loginDetailsActivity.this, false, true);

                        new Thread(new Runnable() {
                            @Override
                            public void run()
                            {

                                try
                                {
                                    phone = countryTXT.getText().toString() + login_mobile_txt.getText().toString();

                                    if (phone.startsWith("+"))
                                        phone = phone.replace("+", "");

                                       phone=login_mobile_txt.getText().toString();

                                    String token=sharedPref.getStringPreference(loginDetailsActivity.this,appConstants.mobileToken_KEY);
                                    String url= urlClass.loginURL;

                                    JSONObject Objct = new JSONObject();
                                    Objct.putOpt("phone", phone);
                                    //Objct.putOpt("password", passTXT.getText().toString().trim());
                                    //Objct.putOpt("deviceToken",token);

                                    //loginRequestOld(url,Objct);
                                    requestClass.objectRequest(Objct,url,"login",getApplicationContext());


                                }
                                catch (Exception xx)
                                {
                                    xx.toString();
                                    stopSpinWheel();
                                }
                            }
                        }).start();
                    }
                }
                catch (Exception xx)
                {
                    xx.toString();
                }
            }
        });



        forgetPasswordTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(loginDetailsActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
            }
        });


        signUpBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(loginDetailsActivity.this, RegisterDetailsActivity.class);
//                startActivity(intent);
            }
        });

        countryTXT.setText("+962");
        flagImg.setBackgroundResource(R.drawable.flag_jo);

        picker = CountryPicker.newInstance(getString(R.string.country));  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                // Implement your code here

                countryTXT.setText(dialCode);
                flagImg.setBackgroundResource(flagDrawableResID);
                picker.dismiss();
            }
        });

        countryTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        flagImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
    }

    private  boolean checkRule() {

        if (login_mobile_txt.getText().toString().trim().equals("")) {
            Toast.makeText(loginDetailsActivity.this, getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }

//        if (passTXT.getText().toString().trim().equals("")) {
//            Toast.makeText(loginDetailsActivity.this, getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
//            return false;
//        }
        return  true;
    }


    private void loginRequestOld(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

         jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
                 {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        stopSpinWheel();
                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONObject obj=jsonObjResp.getJSONObject("data");
                            String userID=obj.getString("userID").toString();
                            String userToken=obj.getString("access_token").toString();
                            String country=obj.getString("country").toString();
                            String notifi=obj.getString("notifications").toString();
                            String nickName=obj.getString("name").toString();
                           appConstants.profilePicture=obj.getString("profilePicture");

                            appConstants.userToken = userToken;
                            appConstants.userID =userID;

                            sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userID_KEY,userID);
                            sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userToken_KEY,userToken);
                            sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userName_KEY,login_mobile_txt.getText().toString().trim());
                            sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userPassword_KEY,passTXT.getText().toString().trim());
                            sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.notificationStatus_KEY,notifi);
                            sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userCountry_KEY,country);
                            sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userNickName,nickName);
                            sharedPref.setBooleanPreference(loginDetailsActivity.this, appConstants.isLoggedIn,true);

                            Intent intent = new Intent(loginDetailsActivity.this, MainScreenActivity.class);
                            startActivity(intent);

                           finish();
                        }
                        else
                        {
                            stopSpinWheel();
                            Toast.makeText(loginDetailsActivity.this, "Login Failed".toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                    else
                    {
                        stopSpinWheel();
                        Toast.makeText(loginDetailsActivity.this, getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(loginDetailsActivity.this,e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(loginDetailsActivity.this,error.getMessage(), Toast.LENGTH_SHORT).show();
                stopSpinWheel();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(loginDetailsActivity.this);
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    public void RequestData(final String IDFB) {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                JSONObject json = response.getJSONObject();
                try {
                    if (json != null) {

                        JsonObjectRequest jsonObjReq;

                       appConstants.startSpinwheel(loginDetailsActivity.this, false, true);


                        JSONObject jo = new JSONObject();
                        jo.putOpt("APIKey", "123456");
                        jo.putOpt("RegistrationType", "1");
                        jo.putOpt("Email", json.getString("email"));
                        jo.putOpt("UserName", json.getString("name"));
                        jo.putOpt("AuthenticationID", json.getString("id"));

                        String urlPost = urlClass.loginSocialMediaURL;

                        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jo, new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                    if (response != null) {
                                        stopSpinWheel();


                                    }
                                }catch (Exception e) {
                                    Toast.makeText(getApplicationContext(), e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                                    stopSpinWheel();
                                }

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                                stopSpinWheel();

                            }
                        });

                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                                appConstants.appTimeOut,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        requestQueue.add(jsonObjReq);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }


    @Override
    public void successRequest(JSONObject response, String apiName) {

        if(apiName.equalsIgnoreCase("login"))
        {
            try
            {
                JSONObject jsonObjResp = new JSONObject(response.toString());
                final String success=jsonObjResp.getString("success");

                if(success.equalsIgnoreCase("true"))
                {
                    JSONObject obj=jsonObjResp.getJSONObject("data");
                    String userID=obj.getString("userId").toString();
                    String otp=obj.getString("otp").toString();
                    String status=obj.getString("status").toString();

                    appConstants.userID =userID;
                    appConstants.otp =otp;
                    sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userID_KEY,userID);
//                    sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userToken_KEY,userToken);
//                    sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userName_KEY,login_mobile_txt.getText().toString().trim());
//                    sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userPassword_KEY,passTXT.getText().toString().trim());
//                    sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.notificationStatus_KEY,notifi);
//                    sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userCountry_KEY,country);
//                    sharedPref.setStringPreference(loginDetailsActivity.this, appConstants.userNickName,nickName);
//                    sharedPref.setBooleanPreference(loginDetailsActivity.this, appConstants.isLoggedIn,true);

                    if(status.equalsIgnoreCase("Pending"))
                    {
                        SliderFragment.editProf=true;
                    }
                    else
                    {
                        SliderFragment.editProf=false;
                    }
                        OTPActivity.phone=phone;
                        Intent intent = new Intent(loginDetailsActivity.this, OTPActivity.class);
                        startActivity(intent);

                }
                else
                {
                    stopSpinWheel();
                    Toast.makeText(loginDetailsActivity.this, "Login Failed".toString(), Toast.LENGTH_LONG).show();
                }

            }
            catch (Exception xx){}
        }

    }

    @Override
    public void volleyError(String msg, String apiName) {

    }
}
