package com.firefly.android.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.firefly.android.app.R;
import com.firefly.android.app.models.AdsClass;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class AdsActivity extends AppCompatActivity {




    AdsClass adsClass_;
    ArrayList<AdsClass> adsClassList_;
    ImageView tutorIMG;
    TextView skipBTN;
    int counter=0;
    SharedPrefsUtils sharedPref;
    boolean isReadTutor=false;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.ads_activity);

        tutorIMG = (ImageView) findViewById(R.id.totorIMG);
        skipBTN = (TextView) findViewById(R.id.skipBTN);
        skipBTN.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {

               // sharedPref.setBooleanPreference(AdsActivity.this, appConstants.isAdsRead_KEY,true);

                Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
                startActivity(i);
                finish();

            }
        });
        tutorIMG.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                counter+=1;
                if(counter<adsClassList_.size())
                {
                    Picasso.with(getApplicationContext()).load(adsClassList_.get(counter).getAdsImgURL())
                            .into(tutorIMG);
                }
                else
                {
                    //sharedPref.setBooleanPreference(AdsActivity.this, appConstants.isAdsRead_KEY,true);


                    Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
                    startActivity(i);
                    finish();
                }


            }
        });


        //isReadTutor=sharedPref.getBooleanPreference(getApplicationContext(), appConstants.isAdsRead_KEY,false);

        if(isReadTutor==false)
        {
            appConstants.startSpinwheel(AdsActivity.this,false,true);
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {

                        String  urlData =""; //urlClass.getAdsURL;

                        getAdsReq(urlData, null);
                    }
                    catch (Exception xx)
                    {}

                }
            }).start();
        }
        else
        {
            Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
            startActivity(i);
            finish();
        }





    }
    private void getAdsReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {


            queue = Volley.newRequestQueue(getApplicationContext());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response.equals("") ||response==null || response.length()==0 )
                            {
                                appConstants.stopSpinWheel();
                                Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
                                startActivity(i);
                                finish();
                            }
                            else
                            {
                                if (response != null)
                                {
                                    try
                                    {
                                        appConstants.stopSpinWheel();

                                        adsClassList_ = new ArrayList<>();


                                        JSONObject result = new JSONObject(response);
                                        JSONArray jsonArrResp = new JSONArray(result.get("data").toString());
                                        for (int j = 0; j < jsonArrResp.length(); j++) {
                                            JSONObject mainObject = jsonArrResp.getJSONObject(j);
                                            String id = mainObject.getString("id");
                                            String name = mainObject.getString("title");
                                            String image = mainObject.getString("img");
                                            image = urlClass.baseURL + image;
                                            String type = mainObject.getString("type");
                                            if (type.equalsIgnoreCase("app open"))
                                            {

                                                adsClass_ = new AdsClass(id, name, image);
                                                adsClassList_.add(adsClass_);

                                            }

                                        }
                                        Picasso.with(getApplicationContext()).load(adsClassList_.get(counter).getAdsImgURL()).into(tutorIMG);

                                    }
                                    catch (Exception xx){}

                                }

                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {
                    String xx = error.toString();
                    appConstants.stopSpinWheel();

                    Intent i = new Intent(getApplicationContext(), MainScreenActivity.class);
                    startActivity(i);
                    finish();



                }


            })


            {



                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<String, String>();
                    try {

                        Iterator<?> keys = jsonObject.keys();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            String value = jsonObject.getString(key);
                            params.put(key, value);

                        }


                    } catch (Exception xx) {
                        xx.toString();
                        appConstants.stopSpinWheel();
                    }
                    return params;
                }


                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    try {

                        String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));

                        return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));


                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    }
                }


            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }






}