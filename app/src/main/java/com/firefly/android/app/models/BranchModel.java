package com.firefly.android.app.models;

import org.json.JSONArray;

/**
 * Created by HP on 3/25/2018.
 */

public class BranchModel {

    private String id;
    private String name;
    private String country;
    private String city;
    private String phone;
    private String address;
    private String logti;
    private String latitude;
    private JSONArray regionArr;

    public BranchModel(String id, String name, String country, String city, String phone, String address, String logti, String latitude, JSONArray regionArr) {
        this.setId(id);
        this.setName(name);
        this.setCountry(country);
        this.setCity(city);
        this.setPhone(phone);
        this.setAddress(address);
        this.setLogti(logti);
        this.setLatitude(latitude);
        this.setRegionArr(regionArr);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogti() {
        return logti;
    }

    public void setLogti(String logti) {
        this.logti = logti;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public JSONArray getRegionArr() {
        return regionArr;
    }

    public void setRegionArr(JSONArray regionArr) {
        this.regionArr = regionArr;
    }
}
