package com.firefly.android.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firefly.android.app.R;
import com.firefly.android.app.activity.CountryActivity;
import com.firefly.android.app.models.CityWithRegionsModel;
import com.firefly.android.app.models.CountryModel;

import java.util.ArrayList;


/**
 * Created by HP on 8/27/2017.
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.MyViewHolder> {



    Context contxt;


    ArrayList<CountryModel> arrayList;

    //int pos=0;
    // ProgressDialog progressBarNew;
    public CountryAdapter( Context context, ArrayList<CountryModel> modelArray) {

        this.contxt=context;
        this.arrayList = modelArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.countries_grid_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.nameTXT.setText(arrayList.get(position).getName());
        holder.codeTXT.setText(arrayList.get(position).getZip());
       // Picasso.with(contxt).load(arrayList.get(holder.getAdapterPosition()).getFlagIMG()).into(holder.flagIMG);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CountryActivity.goToMainScreen(arrayList.get(position).getId());
            }
        });


    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTXT,codeTXT;
        ImageView flagIMG;

        public MyViewHolder(View view) {
            super(view);

            nameTXT = (TextView) view.findViewById(R.id.nameTXT);
            codeTXT= (TextView) view.findViewById(R.id.codeTXT);
            flagIMG= (ImageView) view.findViewById(R.id.flagIMG);



        }
    }




    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

}