package com.firefly.android.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.firefly.android.app.R;
import com.firefly.android.app.models.SliderModel;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by HP on 5/9/2017.
 */

public class SliderFragment extends Fragment implements BaseSliderView.OnSliderClickListener {

    View view;
    SliderLayout sliderLayout;

    ArrayList<SliderModel> bannerArraylist;
    SliderModel sliderModel;

    ImageView orderBTN,menuBTN,cartBTN,pointsBTN;
    public static boolean editProf=false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.slider_screen_layout, container, false);

        sliderLayout=view.findViewById(R.id.sliderLayout);
        menuBTN=view.findViewById(R.id.menuBTN);
        orderBTN=view.findViewById(R.id.orderBTN);
        cartBTN=view.findViewById(R.id.cartBTN);
        pointsBTN=view.findViewById(R.id.pointsBTN);

        //MainScreenActivity.title_img.setImageResource(R.drawable.about_us_txt);

        appConstants.startSpinwheel(getActivity(), false, true);

        menuBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, new MainMenuFragment());
                 tx.addToBackStack(null);
                tx.commit();
            }
        });

        pointsBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, new PointsFragment());
                tx.addToBackStack(null);
                tx.commit();
            }
        });

        cartBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, new MyCartFragment());
                tx.addToBackStack(null);
                tx.commit();
            }
        });

        orderBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, new MyOrderFragment());
                tx.addToBackStack(null);
                tx.commit();
            }
        });


        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getSliderURL;
                    getSliderImageReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();

        if(editProf==true)
        {
            editProf=false;
            FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_container, new MyProfileFragment());
            tx.addToBackStack(null);
            tx.commit();
        }


        return view;
    }


    private void getSliderImageReq(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response)
            {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                        bannerArraylist=new ArrayList<>();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");


                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONArray arr =jsonObjResp.getJSONArray("data");

                            for(int i=0;i<arr.length();i++)
                            {
                                JSONObject obj = arr.getJSONObject(i);
                                String id=obj.getString("id");
                                //String title=obj.getString("title");
                                String image=obj.getString("url");


                                sliderModel =new SliderModel(id,"",image);
                                bannerArraylist.add(sliderModel);

                                DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
                                textSliderView
                                        .description("")
                                        .image(image)
                                        .setScaleType(BaseSliderView.ScaleType.Fit)
                                        .setOnSliderClickListener(SliderFragment.this);
                                textSliderView.bundle(new Bundle());
                                textSliderView.getBundle()
                                        .putString("extra", "");

                                sliderLayout.addSlider(textSliderView);


                            }

                            sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default);
                            sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            sliderLayout.setCustomAnimation(new DescriptionAnimation());
                            sliderLayout.setDuration(3000);
                            //sliderLayout.addOnPageChangeListener(SliderFragment.this);

                        }
                        else
                        {
                            appConstants.stopSpinWheel();
                            Toast.makeText(getActivity(), jsonObjResp.getString("data"), Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();

                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
