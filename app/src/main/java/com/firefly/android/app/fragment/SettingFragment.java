package com.firefly.android.app.fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * Created by HP on 5/3/2017.
 */

public class SettingFragment extends Fragment
{
    View view;

    TextView arBTN,engBTN;
    FragmentTransaction tx;
    SharedPrefsUtils sharedPref;
    Switch pushSwitch;
    boolean isLogggedIn=false;
    String isEnablChecked="";
    Spinner languageSpin;
    ImageView addAddressBTN;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.setting_fragment_layout, container, false);
        pushSwitch=view.findViewById(R.id.pushSwitch);
        languageSpin=view.findViewById(R.id.languageSpin);
        addAddressBTN=view.findViewById(R.id.addAddressBTN);

        MainScreenActivity.toolbar_title.setText(R.string.setting);

        isEnablChecked=sharedPref.getStringPreference(getActivity(), appConstants.notificationStatus_KEY);

        if(isEnablChecked.equalsIgnoreCase("1"))
        {
            pushSwitch.setChecked(true);
        }
        else
        {
            pushSwitch.setChecked(false);
        }



        final String[] lang = getResources().getStringArray(R.array.lang);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, lang);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        languageSpin.setAdapter(spinnerArrayAdapter);


        languageSpin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 1) {
                    setInitialLanguage("ar");
                } else  if (position == 2) {
                    setInitialLanguage("en");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



//        arBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setInitialLanguage("ar");
//
//            }
//        });
//
//        engBTN.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setInitialLanguage("en");
//
//            }
//        });

        addAddressBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_container, new AddAddressFragment());
                tx.addToBackStack(null);
                tx.commit();
            }
        });


        pushSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isLogggedIn) {

                    if (isChecked) {
                        try
                        {

                            sharedPref.setStringPreference(getActivity(),appConstants.notificationStatus_KEY,"1");
                            String url="";
                            postPushStatusReq(url, null);

                        }
                        catch (Exception xx) { }
                    }
                    else {
                        try
                        {
                            sharedPref.setStringPreference(getActivity(),appConstants.notificationStatus_KEY,"0");
                            String url="";
                            postPushStatusReq(url, null);
                        }
                        catch (Exception xx) { }

                    }
                }

            }
        });


        return view;

    }





    private void postPushStatusReq(String urlPost, final JSONObject jsonObject) {

        appConstants.startSpinwheel(getActivity(),false,true);
        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {


            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try
                            {

                                if (response != null)
                                {
                                    JSONArray arr=new JSONArray(response);
                                    JSONObject result=arr.getJSONObject(0);
                                    Toast.makeText(getActivity(), result.get("status").toString(), Toast.LENGTH_SHORT).show();
                                    appConstants.stopSpinWheel();

                                }



                            }
                            catch (Exception xx)
                            {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {
                    String xx = error.toString();
                    Toast.makeText(getActivity(), R.string.connection_err, Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            })


            {



                @Override
                protected Map<String, String> getParams() {

                    Map<String, String> params = new HashMap<String, String>();
                    try {

                        Iterator<?> keys = jsonObject.keys();

                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            String value = jsonObject.getString(key);
                            params.put(key, value);

                        }


                    } catch (Exception xx) {
                        xx.toString();
                        appConstants.stopSpinWheel();
                    }
                    return params;
                }


                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    try {

                        String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));

                        return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));


                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    }
                }


            };

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    private void setInitialLanguage(String language) {
        Locale locale = new Locale(language);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);
        onConfigurationChanged(conf);

        if(language.equalsIgnoreCase("ar"))
        {
            appConstants.current_language="Ar";
        }
        else
        {
            appConstants.current_language="En";
        }


        sharedPref.setStringPreference(getActivity(),appConstants.locale_KEY,language)
        ;
        Intent i = getActivity().getPackageManager()
                .getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        startActivity(i);
    }






}
