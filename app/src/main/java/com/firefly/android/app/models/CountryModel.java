package com.firefly.android.app.models;

/**
 * Created by HP on 3/25/2018.
 */

public class CountryModel {

    private String id;
    private String name;
    private String currency;
    private String tax;
    private String code;
    private String zip;

    public CountryModel(String id, String name, String currency, String tax, String code, String zip) {
        this.id = id;
        this.name = name;
        this.currency = currency;
        this.tax = tax;
        this.code = code;
        this.zip = zip;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
