package com.firefly.android.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.LoginActivity;
import com.firefly.android.app.models.CountryModel;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 5/3/2017.
 */

public class RegisterDetailsActivity extends Activity
{

    EditText nameTXT,emailTXT,passTXT,confirmPassTXT,mobileTXT;
    Spinner countrySpinner;

    Button submitBTN;
    SharedPrefsUtils sharedPref;

    ArrayList<CountryModel> countryArrayList;
    CountryModel countryModel;
    String [] countryArr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_details_fragment_layout);

        initialScreen();
        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


                if(checkRule())
                {
                    appConstants.startSpinwheel(RegisterDetailsActivity.this, false, true);

                    new Thread(new Runnable() {
                        @Override
                        public void run()
                        {

                            try
                            {

                                String url= urlClass.registerURL;
                                String token=sharedPref.getStringPreference(getApplicationContext(),appConstants.mobileToken_KEY);

                                JSONObject registerObjct = new JSONObject();
                                registerObjct.putOpt("name", nameTXT.getText().toString().trim());
                                registerObjct.putOpt("email", emailTXT.getText().toString().trim());
                                registerObjct.putOpt("password", confirmPassTXT.getText().toString().trim());
                                registerObjct.putOpt("phone", mobileTXT.getText().toString().trim());
                                registerObjct.putOpt("country",Integer.parseInt(countryArrayList.get(countrySpinner.getSelectedItemPosition()).getId()));
                                registerObjct.putOpt("deviceToken",token);

                                registrationRequest(url,registerObjct);
                            }
                            catch (Exception xx)
                            {
                                xx.toString();
                                appConstants.stopSpinWheel();
                            }
                        }
                    }).start();
                }
            }
        });



        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getCountryURL;
                    getCountriestReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();
    }



    private void initialScreen()
    {

        nameTXT= (EditText) findViewById(R.id.reg_name_txt);
        emailTXT= (EditText) findViewById(R.id.emailTXT);
        countrySpinner=findViewById(R.id.countrySpinner);
        mobileTXT= (EditText) findViewById(R.id.reg_mobile_txt);
        passTXT= (EditText) findViewById(R.id.reg_password_txt);
        confirmPassTXT= (EditText) findViewById(R.id.reg_confirm_password_txt);
        submitBTN= (Button) findViewById(R.id.submitRgsterBTN);



    }


    private  boolean checkRule()
    {
        if (nameTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }


        if (mobileTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }



        if (mobileTXT.length()<5) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }


        if (passTXT.getText().toString().trim().equals("")) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!confirmPassTXT.getText().toString().trim().equals(passTXT.getText().toString().trim()))
        {
            Toast.makeText(getApplicationContext(), "password not match", Toast.LENGTH_SHORT).show();
            return false;
        }

        return  true;
    }



    private void registrationRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                            JSONObject jsonObjResp = new JSONObject(response.toString());
                            final String status=jsonObjResp.getString("success");

                            if(status.equalsIgnoreCase("true"))
                            {
                                Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_LONG).show();
                                finish();

                            }
                            else
                            {
                                final String status_message=jsonObjResp.getString("status_message");
                                Toast.makeText(getApplicationContext(), status_message, Toast.LENGTH_LONG).show();
                            }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

        private void getCountriestReq(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        countryArrayList=new ArrayList<>();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONArray mainArr=jsonObjResp.getJSONArray("data");
                            countryArr=new String[mainArr.length()];

                            for (int i=0;i<mainArr.length();i++)
                            {

                                JSONObject obj=mainArr.getJSONObject(i);

                                String id=obj.getString("id");
                                String name=obj.getString("name");
                                String currency=obj.getString("currency");
                                String tax=obj.getString("tax");
                                String code=obj.getString("countryCode");
                                String phoneCode=obj.getString("phoneCode");

                                countryModel=new CountryModel(id,name,currency,tax,code,phoneCode);
                                countryArrayList.add(countryModel);
                                countryArr[i]=name;

                            }

                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(RegisterDetailsActivity.this,R.layout.spinner_item, countryArr);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            countrySpinner.setAdapter(spinnerArrayAdapter);


                        }


                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }



}
