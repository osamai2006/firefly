package com.firefly.android.app.fragment;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.adapters.MainMenuAdapter;
import com.firefly.android.app.models.MainMenuModel;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by HP on 5/3/2018.
 */

public class MainMenuFragment extends Fragment  {

    RecyclerView mainmenuLV;

    View view;

    static ArrayList<MainMenuModel> arrayList;
    MainMenuModel model;
    MainMenuAdapter adapter;


    static FragmentTransaction tx;
    private static Fragment fragment;
    LocationManager locationManager;
    Location location;


    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {


            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 11);

            } else {

                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (location != null) {
                    appConstants.latit =location.getLatitude();
                    appConstants.longt = location.getLongitude();

                } else {
                }
            }
        }
        catch (Exception xx){}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_menu_fragment_layout, container, false);
        mainmenuLV= (RecyclerView) view.findViewById(R.id.mainmenuLV);


//        int resId = R.anim.item_animation_fall_down;
//        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(),resId);
//        mainmenuLV.setLayoutAnimation(animation);



        MainScreenActivity.toolbar_title.setText(R.string.menu_tab);


        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getMainMenuURL;
                    getMainMenuReq(url, null);

                } catch (Exception xx)
                {
                   String xxx=xx.toString();
                }
            }
        }).start();


        return view;
    }






    private void getMainMenuReq(String urlPost, final JSONObject jsonObject) {

        final RequestQueue queue;
        StringRequest stringRequest = null;

        try {

            queue = Volley.newRequestQueue(getActivity());

            stringRequest = new StringRequest(Request.Method.GET, urlPost,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try
                            {
                                if(response!=null)
                                {
                                    appConstants.stopSpinWheel();
                                    arrayList=new ArrayList<>();

                                    JSONObject jsonObjResp = new JSONObject(response.toString());
                                    final String status=jsonObjResp.getString("success");

                                    if(status.equalsIgnoreCase("true"))
                                    {
                                        JSONArray arr=jsonObjResp.getJSONArray("data");
                                        for (int i = 0; i < arr.length(); i++)
                                        {
                                            JSONObject obj=arr.getJSONObject(i);
                                            String id = obj.getString("itemId");
                                            String img =obj.getString("image");
                                            String name = obj.getString("name");
                                            String nameImg = obj.getString("imageName");
                                            String price = obj.getString("price");
                                            String mealPrice = obj.getString("mealPrice");
                                            String meat = obj.getString("meat");
                                            String country = obj.getString("country");
                                            JSONArray ingredArr = obj.getJSONArray("ingredients");
                                            JSONArray extrasArr =new JSONArray(); //obj.getJSONArray("burger");

                                            model = new MainMenuModel(id, img, name, nameImg,price,mealPrice, meat, country,ingredArr,extrasArr);
                                            arrayList.add(model);


                                        }

                                        adapter = new MainMenuAdapter(myContext,arrayList);
                                        mainmenuLV.setAdapter(adapter);
                                        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                                        mainmenuLV.setLayoutManager(layoutManager);
                                    }

                                }


                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                appConstants.stopSpinWheel();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();


                }


            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String bearer = "Bearer ".concat(appConstants.userToken);
                    Map<String, String> headersSys = super.getHeaders();
                    Map<String, String> headers = new HashMap<String, String>();
                    headersSys.remove("Authorization");
                    headers.put("Authorization", bearer);
                    headers.put("language", appConstants.current_language);
                    headers.putAll(headersSys);
                    return headers;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    appConstants.appTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(stringRequest);


        } catch (Exception e) {
            appConstants.stopSpinWheel();
            e.toString();
        }



    }

    public static void goToMealDetailsScreen(int position)
    {

        MenuDetailsFragment.name=arrayList.get(position).getName();
        MenuDetailsFragment.price=arrayList.get(position).getPrice();
        MenuDetailsFragment.meat=arrayList.get(position).getMeat();
        MenuDetailsFragment.image=arrayList.get(position).getImage();
        MenuDetailsFragment.mealID=arrayList.get(position).getId();
        MenuDetailsFragment.mealPrice=arrayList.get(position).getMealPrice();

        tx = myContext.getSupportFragmentManager().beginTransaction();
        tx.replace(R.id.main_container, new MenuDetailsFragment());
        tx.addToBackStack(null);
        tx.commit();
    }





}
