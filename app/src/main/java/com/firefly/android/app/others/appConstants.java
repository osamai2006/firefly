package com.firefly.android.app.others;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.text.format.DateFormat;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TimePicker;

import com.firefly.android.app.R;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by HP on 12/24/2017.
 */

public class appConstants {
    public  static String userID_KEY="userID";
    public  static String userToken_KEY="userToken";
    public  static String mobileToken_KEY="token_key";
    public  static String userCountry_KEY="userCountry_KEY";
    public  static String notificationStatus_KEY="notificationStatus_KEY";
    public  static String locale_KEY="local_KEY";
    public  static String current_language="";
    public  static String userName_KEY="userName";
    public  static String userNickName="userNickName";
    public  static String userPassword_KEY="userPass";
    public  static String userType_KEY="userType";
    public  static String isLoggedIn="isloggedIn";
    public  static int appTimeOut=6000;
    public  static String IMAGE_DIRECTORY_NAME="";
    public  static String profilePicture="";
    public  static String countryID="";
    public  static String comeFromdelivery="";
    public  static String addressID="";




    public  static double longt=0.0;
    public  static double latit=0.0;
    public  static String userToken="";
    public  static String userID="";
    public  static String otp="";
    public  static String checkDayID_KEY="checkDayID_KEY";
    public  static String checkBreakID_KEY="checkBreakID_KEY";

    public  static String config_leaves_monthly="";
    public  static String config_vacation_monthly="";
    public  static String config_loans_monthly="";


    public static final String email_pattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";




    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception xx) {
            xx.toString();
        }
    }


    public static Dialog spinWheelDialog = null;
    public static void startSpinwheel(Context context, boolean setDefaultLifetime, boolean isCancelable) {
        try {

            if (spinWheelDialog != null && spinWheelDialog.isShowing())
                return;

            spinWheelDialog = new Dialog(context, R.style.wait_spinner_style);
            ProgressBar progressBar = new ProgressBar(context);
            ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
            spinWheelDialog.addContentView(progressBar, layoutParams);
            spinWheelDialog.setCancelable(isCancelable);
            spinWheelDialog.show();

            Handler spinWheelTimer = new Handler();
            spinWheelTimer.removeCallbacks(dismissSpinner);
            if (setDefaultLifetime) // If requested for default dismiss time.
                spinWheelTimer.postAtTime(dismissSpinner, SystemClock.uptimeMillis() + 1000);

            spinWheelDialog.setCanceledOnTouchOutside(false);
        }
        catch (Exception xx)
        {}
    }
    static Runnable dismissSpinner = new Runnable() {

        @Override
        public void run() {
            stopSpinWheel();
        }

    };

    public static void stopSpinWheel() {
        if (spinWheelDialog != null)
            try
            {
                spinWheelDialog.dismiss();
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        spinWheelDialog = null;
    }

    public static String currentDate(Boolean Time,Boolean Date,Boolean DateTime) {

        String FulDate = null;
        final Calendar c = Calendar.getInstance();
        int hour=c.get(Calendar.HOUR);
        int mint=c.get(Calendar.MINUTE);
        int sec=c.get(Calendar.SECOND);

        int todaysDate = (c.get(Calendar.YEAR) * 10000) +((c.get(Calendar.MONTH) + 1) * 100) + (c.get(Calendar.DAY_OF_MONTH));
        String DateString=String.valueOf(todaysDate);
        String Year=DateString.substring(0, 4);
        String Month=DateString.substring(4, 6);
        String Day=DateString.substring(6, 8);

        if(Date==true)
        {
            FulDate=Year+"-"+Month+"-"+Day;
        }
        else if(Time==true)
        {
            FulDate=hour+":"+mint+":"+sec;
        }
        else if(DateTime==true)
        {
            FulDate=Day+"-"+Month+"-"+Year+" "+hour+":"+mint+":"+sec;
        }
        return(String.valueOf(FulDate));

    }

    public static String getDate(long timeStamp){

        try{

            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(timeStamp * 1000L);
            String date="";

                date = DateFormat.format("dd-MM-yyyy/hh:mm a", cal).toString();

            return  date;
        }
        catch(Exception ex)
        {
            return "";
        }
    }



}
