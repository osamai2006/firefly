package com.firefly.android.app.fragment;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.activity.SplashActivity;
import com.firefly.android.app.models.BranchModel;
import com.firefly.android.app.others.SharedPrefsUtils;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class DeliveryFragmentNew extends Fragment implements OnMapReadyCallback,GoogleMap.OnMapLongClickListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{


    //Spinner branchSspin,regionSspin,countrySpin;
    EditText adressNameTXT, buildingNOTXT, flatNOTXT, apartmentTXT;


//    static ArrayList<BranchModel> branchArrayList;
//    BranchModel branchModel;
//    public static String [] branchArr;
//
//    static ArrayList<RegionsModel> regionsArrayList;
//    RegionsModel regionsModel;
//    public static String [] regionsArr;

    Button submitOrderBTN,currentLocBTN;
    EditText notesTXT;

    public static String checkoutMethod="";
    SharedPrefsUtils sharedPref;
    String mobileNum="";
    LocationManager locationManager;

     SupportMapFragment mapFrag;
    private GoogleMap mGoogleMap;
    private GoogleApiClient googleApiClient;

    public static double selectedlatti = 0.0;
    public static double selectedlongi = 0.0;


    public static JSONObject addressObj=new JSONObject();


    public static MainScreenActivity myContext;
    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(requestCode==11)
        {
            try {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {

                    googleApiClient.registerConnectionCallbacks(this);

                }
                else
                {

                  getActivity().getSupportFragmentManager().popBackStack();
                }

            }
            catch (Exception xx){}
        }


    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {


            if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 11);

            } else {
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    appConstants.latit =location.getLatitude();
                    appConstants.longt =location.getLongitude();


                } else {
                }
            }
        }
        catch (Exception xx){}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v=inflater.inflate(R.layout.delivery_layout, container, false);


        MainScreenActivity.toolbar_title.setText(R.string.deleviry);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        googleApiClient = new GoogleApiClient.Builder(getActivity(), DeliveryFragmentNew.this, DeliveryFragmentNew.this).addApi(LocationServices.API).build();



        mapFrag = (SupportMapFragment)  getChildFragmentManager().findFragmentById(R.id.addressMap);

        currentLocBTN= v.findViewById(R.id.currentLocBTN);
        mobileNum = sharedPref.getStringPreference(getActivity(), appConstants.userName_KEY);

        submitOrderBTN=v.findViewById(R.id.submitOrderBTN);

       // timeTXT=v.findViewById(R.id.timeTXT);
        notesTXT=v.findViewById(R.id.notesTXT);

        adressNameTXT = v.findViewById(R.id.adressNameTXT);
       // postalCodeTXT = v.findViewById(R.id.postalCodeTXT);
        buildingNOTXT =  v.findViewById(R.id.buildingNOTXT);
        flatNOTXT =  v.findViewById(R.id.flatNOTXT);
        apartmentTXT=  v.findViewById(R.id.apartmentTXT);




        submitOrderBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkRule())
                {
                    ConfirmationFragmentNew.checkoutMethod="1";
                    ConfirmationFragmentNew.deliveryAddressName=adressNameTXT.getText().toString().trim();
                    ConfirmationFragmentNew.deliveryBuildingNum=buildingNOTXT.getText().toString().trim();

                    ConfirmationFragmentNew.deleveryPrice="";

                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new ConfirmationFragmentNew());
                    tx.addToBackStack(null);
                    tx.commit();

//                    appConstants.startSpinwheel(getActivity(), false, true);
//
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run()
//                        {
//
//                            try
//                            {
//
//                                String url= urlClass.deleverycheckOutURL;
//
//                                JSONObject objct = new JSONObject();
//                                JSONObject geoObj = new JSONObject();
//
//                                addressObj.putOpt("title",adressNameTXT.getText().toString().trim());
//                                addressObj.putOpt("buildingNumber",buildingNOTXT.getText().toString().trim());
//                                //addressObj.putOpt("postalCode",postalCodeTXT.getText().toString().trim());
//
//                                geoObj.putOpt("latitude",appConstants.latit);
//                                geoObj.putOpt("longitude",appConstants.longt);
//                                addressObj.putOpt("geo",geoObj);
//
//
//                               // objct.putOpt("branchId", branchArrayList.get(branchSspin.getSelectedItemPosition()).getId());
//                                //objct.putOpt("regionId", regionsArrayList.get(regionSspin.getSelectedItemPosition()).getId());
//                               // objct.putOpt("deliveryTime", timeTXT.getText().toString().trim());
//                                objct.putOpt("deliveryMethod","1");
//                                objct.putOpt("address",addressObj);
//
//
//                                checkOutRequest(url,objct);
//                            }
//                            catch (Exception xx)
//                            {
//                                xx.toString();
//                                appConstants.stopSpinWheel();
//                            }
//                        }
//                    }).start();
                }

            }
        });




        currentLocBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(appConstants.longt==0.0)
                    {
                        Toast.makeText(getContext(), getResources().getString(R.string.enable_gps), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
                    List<Address> addresses = geo.getFromLocation( appConstants.latit ,appConstants.longt, 1);

                    if (addresses.size() > 0) {

                        adressNameTXT.setText(addresses.get(0).getAddressLine(0));


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        return v;


    }


    private  boolean checkRule()
    {
//        if (nameTxt.getText().toString().trim().equals("")) {
//            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
//            return false;
//        }
//
//
//        if (mobileTxt.getText().toString().trim().equals("")) {
//            Toast.makeText(getContext(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
//            return false;
//        }



//        if (timeTXT.getText().toString().trim().equals(""))
//        {
//            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
//            return false;
//        }

        if (adressNameTXT.getText().toString().trim().equals(""))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_empty), Toast.LENGTH_SHORT).show();
            return false;
        }


        return  true;
    }



    private void checkOutRequest(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {

                            JSONObject obj=jsonObjResp.getJSONObject("data");
                            String id=obj.getString("id");
                            String name=obj.getString("name");
                            String phone=obj.getString("phone");
                            String orderNum=obj.getString("orderNumber");
                            String orderDate=obj.getString("orderDate");
                            String orderTime=obj.getString("orderTime");
                            String orderType=obj.getString("orderType");
                            String deliveryPrice=obj.getString("deliveryPrice");
                            String tax=obj.getString("tax");
                            String orderTotal=obj.getString("orderTotal");

                            try
                            {
                                JSONObject objct = new JSONObject();

                                objct.putOpt("orderId", id);
                                objct.putOpt("accepted", "1");

                                placeOrderRequest(urlClass.placeOrderURL,objct);
                            }
                            catch (Exception xx){}




                        }
                        else
                        {
                            final String status_message=jsonObjResp.getString("status_message");
                            Toast.makeText(getActivity(), status_message, Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }

    private void placeOrderRequest(final String urlPost, final JSONObject jsonObjectRequest) {

        appConstants.startSpinwheel(getActivity(),false,true);
        new Thread(new Runnable() {
            @Override
            public void run() {

                JsonObjectRequest jsonObjReq;

                jsonObjReq = new JsonObjectRequest(Request.Method.POST, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
                {

                    @Override
                    public void onResponse(JSONObject response) {

                        try
                        {
                            if(response !=null)
                            {
                                appConstants.stopSpinWheel();

                                JSONObject jsonObjResp = new JSONObject(response.toString());
                                final String status=jsonObjResp.getString("success");

                                if(status.equalsIgnoreCase("true"))
                                {

                                    JSONObject obj=jsonObjResp.getJSONObject("data");

                                    String orderId=obj.getString("orderId");

                                    InvoiceFragment.orderID=orderId;
                                    InvoiceFragment.invoiceFlag="1";

                                    myContext.getSupportFragmentManager().popBackStack();

                                    FragmentTransaction tx = myContext.getSupportFragmentManager().beginTransaction();
                                    tx.replace(R.id.main_container, new InvoiceFragment());
                                    //tx.addToBackStack(null);
                                    tx.commit();





                                }
                                else
                                {
                                    final String status_message=jsonObjResp.getString("success");
                                    Toast.makeText(getActivity(), status_message, Toast.LENGTH_LONG).show();
                                }

                            }
                            else
                            {
                                appConstants.stopSpinWheel();
                                Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            }


                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getActivity(),e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            appConstants.stopSpinWheel();
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        Toast.makeText(getActivity(),error.getMessage(), Toast.LENGTH_SHORT).show();
                        appConstants.stopSpinWheel();

                    }
                }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        String bearer = "Bearer ".concat(appConstants.userToken);
                        Map<String, String> headersSys = super.getHeaders();
                        Map<String, String> headers = new HashMap<String, String>();
                        headersSys.remove("Authorization");
                        headers.put("Authorization", bearer);
                        headers.putAll(headersSys);
                        return headers;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                        appConstants.appTimeOut,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                requestQueue.add(jsonObjReq);


            }
        }).start();

    }




    public void onMapReady(GoogleMap googleMap) {


        mGoogleMap = googleMap;
        if (mGoogleMap != null)
        {
            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(appConstants.latit, appConstants.longt)).zoom(13).build();
            MarkerOptions marker = new MarkerOptions().position(new LatLng(appConstants.latit, appConstants.longt)).title("Your Location");
            marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_new));

            googleMap.addMarker(marker);
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

        mGoogleMap.setOnMapLongClickListener(this);
    }


    @Override
    public void onMapLongClick(LatLng point) {

        try {
            Geocoder geo = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(point.latitude, point.longitude, 1);

            if (addresses.size() > 0) {



                selectedlongi = addresses.get(0).getLongitude();
                selectedlatti = addresses.get(0).getLatitude();

                adressNameTXT.setText(addresses.get(0).getAddressLine(0));


                MarkerOptions marker = new MarkerOptions().position(new LatLng(point.latitude, point.longitude)).title(addresses.get(0).getCountryName());
                mGoogleMap.clear();

                mGoogleMap.addMarker(marker);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        if (googleApiClient != null)
        {
            googleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        try {


            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

                if(lastLocation!=null)
                {
                    appConstants.stopSpinWheel();
                    double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();


                    appConstants.latit = lat;
                    appConstants.longt = lon;



                    mapFrag.getMapAsync(this);
                }
                else
                {
                    appConstants.startSpinwheel(getActivity(),false,true);
                    googleApiClient.registerConnectionCallbacks(DeliveryFragmentNew.this);
                }





            }
        }
        catch(Exception xx)
        {
            getActivity().registerReceiver(mGpsSwitchStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));

            Toast.makeText(getContext(), getResources().getString(R.string.enable_gps), Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private BroadcastReceiver mGpsSwitchStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED") ||
                    intent.getAction().matches("android.location.LocationManager.PROVIDERS_CHANGED_ACTION"))
            {

                googleApiClient.registerConnectionCallbacks(DeliveryFragmentNew.this);

            }
        }
    };
}
