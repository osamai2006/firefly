package com.firefly.android.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firefly.android.app.R;
import com.firefly.android.app.fragment.MyOrderFragment;
import com.firefly.android.app.models.OrdersModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by HP on 8/27/2017.
 */

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.MyViewHolder> {



    Context context;


    ArrayList<OrdersModel> arrayList;

    //int pos=0;
    // ProgressDialog progressBarNew;
    public OrdersAdapter(Context context, ArrayList<OrdersModel> modelArray) {

        this.context = context;
        this.arrayList = modelArray;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_order_grid_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.dateTXT.setText(arrayList.get(position).getDate());
        holder.priceTXT.setText(arrayList.get(position).getTotal()+" JD");
        holder.qtyTXT.setText(" ---X"+arrayList.get(position).getQty());

        if(arrayList.get(position).getItemsArr().length()>0)
        {
            JSONArray arr=arrayList.get(position).getItemsArr();
            for(int i=0;i<arr.length();i++)
            {
                try {
                    JSONObject obj = arr.getJSONObject(i);

                    String name = obj.getString("name");
                    holder.descTXT.setText(name+",");

                }
                catch (Exception xx){}
            }
            if( holder.descTXT.getText().toString().endsWith(","))
            {
                holder.descTXT.setText(holder.descTXT.getText().toString().substring(0, holder.descTXT.getText().toString().length() - 1));
            }
        }

        holder.detailsBTN.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                MyOrderFragment.goToOrderReq(arrayList.get(position).getId(),position);
            }
        });

        holder.reorderBTN.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {



            }
        });

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView dateTXT,descTXT,priceTXT,qtyTXT;
        Button detailsBTN,reorderBTN;

        public MyViewHolder(View view) {
            super(view);

            dateTXT = (TextView) view.findViewById(R.id.dateTXT);
            descTXT = (TextView) view.findViewById(R.id.descTXT);
            qtyTXT = (TextView) view.findViewById(R.id.qtyTXT);
            detailsBTN = (Button) view.findViewById(R.id.detailsBTN);
            reorderBTN = (Button) view.findViewById(R.id.reorderBTN);
            priceTXT= (TextView) view.findViewById(R.id.priceTXT);



        }
    }




    @Override
    public int getItemCount()
    {
        return arrayList.size();
    }

}