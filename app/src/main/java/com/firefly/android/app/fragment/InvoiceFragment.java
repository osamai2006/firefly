package com.firefly.android.app.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.firefly.android.app.R;
import com.firefly.android.app.activity.MainScreenActivity;
import com.firefly.android.app.adapters.InvoiceAdapter;
import com.firefly.android.app.models.BillModel;
import com.firefly.android.app.others.appConstants;
import com.firefly.android.app.others.urlClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by HP on 5/3/2018.
 */

public class InvoiceFragment extends Fragment  {
    public static MainScreenActivity myContext;
    RecyclerView invoiceLV;
    public static String orderID="";
    public static String invoiceFlag="";
    public static int position;

    View view;

    static ArrayList<BillModel> arrayList;
    BillModel model;
    static InvoiceAdapter adapter;


    static FragmentTransaction tx;
    private static Fragment fragment;

    Button accept_invoiceBTN;
    TextView deleveryTXT,taxTXT,totalTXT;


    @Override
    public void onAttach(Context context) {
        myContext=(MainScreenActivity) getActivity();
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.invoice_fragment_layout, container, false);
        invoiceLV = (RecyclerView) view.findViewById(R.id.invoiceLV);
        accept_invoiceBTN = view.findViewById(R.id.accept_invoiceBTN);
        totalTXT = view.findViewById(R.id.totalTXT);
        deleveryTXT = view.findViewById(R.id.deleveryTXT);
        taxTXT = view.findViewById(R.id.taxTXT);


        MainScreenActivity.toolbar_title.setText(R.string.bill_txt);

        if (invoiceFlag.equalsIgnoreCase("2"))
        {
            accept_invoiceBTN.setText(getResources().getString(R.string.reorder));

        } else
        {
            accept_invoiceBTN.setText(getResources().getString(R.string.finish));
            MainScreenActivity.getCartCountReq();
        }


        appConstants.startSpinwheel(getActivity(), false, true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    String url = urlClass.getBillURL+orderID+"/bill";
                    getBillReq(url, null);

                } catch (Exception xx)
                {
                    String xxx=xx.toString();
                }
            }
        }).start();


        accept_invoiceBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (invoiceFlag.equalsIgnoreCase("2"))
                {
                    new AlertDialog.Builder(getActivity())
                            .setIcon(R.mipmap.icon)
                            .setMessage(R.string.reorder_msg)
                            .setCancelable(false)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    MyOrderFragment.reOrderReq(orderID,position);
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();


                }
                else
                {
                    FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_container, new MainMenuFragment());
                   // tx.addToBackStack(null);
                    tx.commit();
                }

            }
        });
        return view;
    }



    private void getBillReq(String urlPost, final JSONObject jsonObjectRequest) {

        JsonObjectRequest jsonObjReq;

        jsonObjReq = new JsonObjectRequest(Request.Method.GET, urlPost, jsonObjectRequest, new Response.Listener<JSONObject>()
        {

            @Override
            public void onResponse(JSONObject response) {

                try
                {
                    if(response !=null)
                    {
                        appConstants.stopSpinWheel();
                        arrayList=new ArrayList<>();

                        JSONObject jsonObjResp = new JSONObject(response.toString());
                        final String status=jsonObjResp.getString("success");

                        if(status.equalsIgnoreCase("true"))
                        {
                            JSONObject mainObject=jsonObjResp.getJSONObject("data");
                            deleveryTXT.setText(mainObject.getString("deliveryPrice")+" JD");
                            taxTXT.setText(mainObject.getString("tax"));
                            totalTXT.setText(mainObject.getString("orderTotal")+" JD");

                            JSONArray itemsArr=mainObject.getJSONArray("items");

                            for (int i=0;i<itemsArr.length();i++)
                            {

                                JSONObject obj=itemsArr.getJSONObject(i);

                                String itemID="";//obj.getString("itemId");
                                String name=obj.getString("name");
                                String qty=obj.getString("quantity");
                                String price=obj.getString("price");
                                String note=obj.getString("note");
                                JSONArray ingrediant=new JSONArray();
                                try{ingrediant=obj.getJSONArray("ingredients");}
                                catch (Exception xx){}

                                JSONArray extra=new JSONArray();
                                try{extra=obj.getJSONArray("extras");}
                                catch (Exception xx){}

                                JSONObject drinks=new JSONObject();
                                try{drinks=obj.getJSONObject("drinks");}
                                catch (Exception xx){}

                                JSONObject potato=new JSONObject();
                                try{potato=obj.getJSONObject("potato");}
                                catch (Exception xx){}

                                model=new BillModel(itemID,name,qty,price,note,ingrediant,extra,drinks,potato);
                                arrayList.add(model);

                            }

                            adapter = new InvoiceAdapter(myContext,arrayList);
                            invoiceLV.setAdapter(adapter);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            invoiceLV.setLayoutManager(layoutManager);

                        }


                    }
                    else
                    {
                        appConstants.stopSpinWheel();
                        Toast.makeText(getActivity(), getResources().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    }


                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    appConstants.stopSpinWheel();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Toast.makeText(getActivity(),getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                appConstants.stopSpinWheel();

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String bearer = "Bearer ".concat(appConstants.userToken);
                Map<String, String> headersSys = super.getHeaders();
                Map<String, String> headers = new HashMap<String, String>();
                headersSys.remove("Authorization");
                headers.put("Authorization", bearer);
                headers.put("language", appConstants.current_language);
                headers.putAll(headersSys);
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                appConstants.appTimeOut,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjReq);
    }



}
