package com.firefly.android.app.models;

/**
 * Created by HP on 5/21/2017.
 */

public class SliderModel
{
    private String id;
    private String title;
    private String image;

    public SliderModel(String id, String title, String image) {
        this.setId(id);
        this.setTitle(title);
        this.setImage(image);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
